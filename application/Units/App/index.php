<?php
/**
 * Index
 */

define('UNIT_NAME', 'App');
define('DEFAULT_MODULE', 'Application');

define('TEMPLATE_PATH', $unitPath . '/' . TEMPLATE_DIR);

ob_start();

$app = new \Core\Application();
$app->go();

