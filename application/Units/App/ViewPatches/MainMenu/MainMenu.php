<?php

namespace App\ViewPatches\MainMenu;

use Core\ViewPatch\ViewPatchAbstract;

class MainMenu extends ViewPatchAbstract
{
    /**
     * Processes view patch
     *
     * @return string
     */
    public function process()
    {
        $fileName = __DIR__ . '/template.phtml';

        if (!is_file($fileName)) {
            return null;
        }

        ob_start();

        include $fileName;

        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

}