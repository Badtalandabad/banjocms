<?php

namespace App\Modules\Tuner\Views\Tuner;

use App\Modules\Tuner\Repositories\TunerRepository;

/**
 * Class TunerView
 */
class TunerView extends \Core\View\ViewAbstract
{
    /**
     * List of tracks
     *
     * @var \App\Modules\Tuner\Repositories\Track[]
     */
    public $tracks = [];

    /**
     * List of artists
     *
     * @var \App\Modules\Tuner\Repositories\Artist[]
     */
    public $artists = [];

    /**
     * List of all music suppliers
     *
     * @var \App\Modules\Tuner\Repositories\MusicSupplier[]
     */
    public $suppliers = [];

    /**
     * Track
     *
     * @var \App\Modules\Tuner\Repositories\Track
     */
    public $track;

    /**
     * Artist
     *
     * @var \App\Modules\Tuner\Repositories\Artist
     */
    public $artist;

    /**
     * Supplier
     *
     * @var \App\Modules\Tuner\Repositories\MusicSupplier
     */
    public $supplier;

    protected function prepareTemplate()
    {
        $rep = TunerRepository::getInstance();

        switch ($this->getTemplate()) {
            case 'default':
            case 'tracks':
                $this->tracks = $rep->getLastTracks();
                if (!is_array($this->tracks)) {
                    \SReporter::addMessage('Can\' receive tracks. Something went wrong.');
                }
                break;
            case 'artists':
                $this->artists = $rep->getLastArtists();
                if (!is_array($this->artists)) {
                    \SReporter::addMessage('Can\' receive artists. Something went wrong.');
                }
                break;
            case 'edit-track':
                if ($trackId = (int)\SRequester::getParam('track-id')) {
                    $this->track = $rep->getTrack($trackId);
                    if (empty($this->track)) {
                        \SReporter::addMessage('Can\' receive track. Something went wrong.');
                        \SRouter::redirect(['module' => 'tuner', 'template' => 'tracks']);
                    }
                }
                //No break needed
            case 'suppliers':
                $this->suppliers = $rep->getAllSuppliers();
                if (!is_array($this->suppliers)) {
                    \SReporter::addMessage('Can\' receive suppliers. Something went wrong.');
                }
                break;
            case 'edit-artist':
                if ($artistId = (int)\SRequester::getParam('artist-id')) {
                    $this->artist = $rep->getArtist($artistId);
                    if (empty($this->artist)) {
                        \SReporter::addMessage('Can\' receive artist. Something went wrong.');
                        \SRouter::redirect(['module' => 'tuner', 'template' => 'artists']);
                    }
                }
                break;
            case 'edit-supplier':
                if ($supplierId = (int)\SRequester::getParam('supplier-id')) {
                    $this->supplier = $rep->getSupplier($supplierId);
                    if (empty($this->supplier)) {
                        \SReporter::addMessage('Can\' receive music supplier. Something went wrong.');
                        \SRouter::redirect(['module' => 'tuner', 'template' => 'suppliers']);
                    }
                }
                break;
        }
    }
}