<?php

/** @var \App\Modules\Tuner\Repositories\MusicSupplier $supplier */
$supplier = $this->supplier;

$title = $supplier ? 'Edit supplier' : 'Add supplier';
$this->document->setTitle($title);

$name = $supplier ? htmlspecialchars($supplier->getName()) : '';
$homeLink = $supplier ? htmlspecialchars($supplier->getHomeLink()) : '';

$action = \SRouter::route(['module' => 'tuner', 'task' => 'save-supplier',]);
?><form action="<?= $action; ?>" method="post">
    <?php if ($supplier): ?>
        <input type="hidden" name="supplier_id" value="<?= (int)$supplier->getId(); ?>">
    <?php endif; ?>
    <label for="supplier-name" class="form-required">Supplier name</label>
    <input type="text" class="input" name="supplier_name" id="supplier-name" value="<?= $name; ?>">
    <label for="supplier-home-link">Supplier home link</label>
    <input type="text" class="input" name="supplier_home_link" id="supplier-home-link" value="<?= $homeLink; ?>">
    <div class="b-form-buttons">
        <button class="button" type="submit">Save</button>
        <button class="button" type="submit" name="edit_more" value="1">Save and keep editing</button>
        <button class="button" type="submit" name="add_new" value="1">Save and add new</button>
    </div>
</form>