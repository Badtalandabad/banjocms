<?php
$this->document->setTitle('Artists');
$addLink = \SRouter::route(['module' => 'tuner', 'template' => 'edit-artist',]);
?>
<a href="<?= $addLink; ?>" class="b-add-entity-button">Add artist</a>
<?php if ($this->artists): ?>
<table class="table">
    <tr>
        <th>Public Name</th>
        <th>Description</th>
        <th></th>
        <th></th>
    </tr>
    <?php
    /** @var \App\Modules\Tuner\Repositories\Artist $artist */
    foreach ($this->artists as $artist):
        $editLink = \SRouter::route([
            'module' => 'tuner',
            'template' => 'edit-artist',
            'artist-id' => $artist->getId(),
        ]);
        $deleteLink = \SRouter::route([
            'module' => 'tuner',
            'task' => 'delete-artist',
            'artist-id' => $artist->getId(),
        ]);
        $jsDeleteWarning = "return confirm('Delete " . htmlspecialchars($artist->getPublicName()) . ". Are you sure?');"
        ?>
        <tr>
            <td><?= htmlspecialchars($artist->getPublicName()); ?></td>
            <td><?= htmlspecialchars($artist->getDescription()); ?></td>
            <td><a href="<?= htmlspecialchars($editLink); ?>">edit</a></td>
            <td><a href="<?= htmlspecialchars($deleteLink); ?>" onclick="<?= $jsDeleteWarning; ?>">delete</a></td>
        </tr>
    <?php endforeach; ?>
</table>
<?php endif; ?>