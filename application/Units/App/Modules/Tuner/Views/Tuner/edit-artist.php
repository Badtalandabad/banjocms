<?php

/** @var \App\Modules\Tuner\Repositories\Artist $artist */
$artist = $this->artist;

$title = $artist ? 'Edit artist' : 'Add artist';
$this->document->setTitle($title);

$name = $artist ? htmlspecialchars($artist->getPublicName()) : '';
$description = $artist ? htmlspecialchars($artist->getDescription()) : '';

$action = \SRouter::route(['module' => 'tuner', 'task' => 'save-artist',]);
?><form action="<?= $action; ?>" method="post">
    <?php if ($artist): ?>
        <input type="hidden" name="artist_id" value="<?= (int)$artist->getId(); ?>">
    <?php endif; ?>
    <label for="artist-public-name" class="form-required">Artist name</label>
    <input type="text" class="input" name="artist_public_name" id="artist-public-name" value="<?= $name; ?>">
    <label for="artist-description">Artist Description</label>
    <textarea class="input" name="artist_description" id="artist-description" cols="30" rows="10"><?= $description; ?></textarea>
    <div class="b-form-buttons">
        <button class="button" type="submit">Save</button>
        <button class="button" type="submit" name="edit_more" value="1">Save and keep editing</button>
        <button class="button" type="submit" name="add_new" value="1">Save and add new</button>
    </div>
</form>