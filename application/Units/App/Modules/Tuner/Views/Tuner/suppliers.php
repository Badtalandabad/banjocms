<?php
$this->document->setTitle('Music suppliers');
$addLink = \SRouter::route(['module' => 'tuner', 'template' => 'edit-supplier',]);
?>
<a href="<?= $addLink; ?>" class="b-add-entity-button">Add music supplier</a>
<?php if (!$this->suppliers): ?>
    No suppliers in the database
    <?php
    return;
endif; ?>
<table class="table">
    <tr>
        <th>Name</th>
        <th>Website</th>
    </tr>
    <?php
    /** @var \App\Modules\Tuner\Repositories\MusicSupplier $supplier */
    foreach ($this->suppliers as $supplier):
        $editLink = \SRouter::route([
            'module' => 'tuner',
            'template' => 'edit-supplier',
            'supplier-id' => $supplier->getId(),
        ]);
        $deleteLink = \SRouter::route([
            'module' => 'tuner',
            'task' => 'delete-supplier',
            'supplier-id' => $supplier->getId(),
        ]);
        $jsDeleteWarning = "return confirm('Delete " . htmlspecialchars($supplier->getName()) . ". Are you sure?');";
        ?>
        <tr>
            <td><?= htmlspecialchars($supplier->getName()); ?></td>
            <td>
                <a href="<?= htmlspecialchars($supplier->getHomeLink()); ?>"
                   target="_blank"><?= htmlspecialchars($supplier->getHomeLink()); ?></a>
            </td>
            <td><a href="<?= htmlspecialchars($editLink); ?>">edit</a></td>
            <td><a href="<?= htmlspecialchars($deleteLink); ?>" onclick="<?= $jsDeleteWarning; ?>">delete</a></td>
        </tr>
    <?php endforeach; ?>
</table>
