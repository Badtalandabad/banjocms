<?php

/** @var \App\Modules\Tuner\Repositories\Track $track */
$track = $this->track;

/** @var \App\Modules\Tuner\Repositories\MusicSupplier[] $suppliers */
$suppliers = $this->suppliers;

$title = $track ? 'Edit track' : 'Add track';
$this->document->setTitle($title);

$trackName = $track ? htmlspecialchars($track->getName()) : '';
$artistName = $track ? htmlspecialchars($track->getArtist()->getPublicName()) : '';
$trackSuppliers = $track ?? $track->getMusicSuppliers();
$supplierLink = '';
$trackComment = $track ? htmlspecialchars($track->getComment()) : '';

$action = \SRouter::route(['module' => 'tuner', 'task' => 'save-track',]);
?><form action="<?= $action; ?>" method="post">
    <?php if ($track): ?>
        <input type="hidden" name="track_id" value="<?= (int)$track->getId(); ?>">
    <?php endif; ?>
    <div>
        <label for="track-name" class="form-required">Track name</label>
        <input type="text" class="input" name="track_name" id="track-name" value="<?= $trackName; ?>">
    </div>
    <div>
        <label for="artist-name" class="form-required">Artist name</label>
        <input type="text" class="input" name="track_artist" id="artist-name" value="<?= $artistName; ?>">
    </div>
    <div>
        <span>Music suppliers</span>
        <?php foreach ($suppliers as $supplier):
            $supplierName = htmlspecialchars($supplier->getName());
            $supplierNameSmall = strtolower($supplierName);
            ?>
            <div>
                <label for="music-suppliers-<?= $supplierNameSmall; ?>">
                    <input type="checkbox" name="music_suppliers_<?= $supplierNameSmall; ?>"
                           id="music-suppliers-<?= $supplierNameSmall; ?>" value="1">&nbsp;<?= $supplierName; ?>
                </label>
                <fieldset class="" disabled="disabled" style="display:none;">
                    <label for="id-on-supplier-side"></label>
                    <input type="text" name="id_on_supplier_side" id="id-on-supplier-side" value="">
                </fieldset>
            </div>
        <?php endforeach; ?>
    </div>
    <div>
        <label for="track-comment">Track comment</label>
        <textarea class="input" name="track_comment" id="track-comment"
                  cols="30" rows="10"><?= $trackComment; ?></textarea>
    </div>
    <div class="b-form-buttons">
        <button class="button" type="submit">Save</button>
        <button class="button" type="submit" name="edit_more" value="1">Save and keep editing</button>
        <button class="button" type="submit" name="add_new" value="1">Save and add new</button>
    </div>
</form>