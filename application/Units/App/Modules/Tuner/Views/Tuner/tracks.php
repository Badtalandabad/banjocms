<?php
$this->document->setTitle('Tracks');
$addLink = \SRouter::route(['module' => 'tuner', 'template' => 'edit-track',]);
?>
<a href="<?= $addLink; ?>" class="b-add-entity-button">Add track</a>
<?php if ($this->tracks): ?>
<table class="table">
    <tr>
        <th>Name</th>
        <th>Artist</th>
        <th>Comment</th>
        <th></th>
        <th></th>
    </tr>
    <?php
    /** @var \App\Modules\Tuner\Repositories\Track $track */
    foreach ($this->tracks as $track):
        $editLink = \SRouter::route([
            'module' => 'tuner',
            'template' => 'edit-track',
            'track-id' => $track->getId(),
        ]);
        $deleteLink = \SRouter::route([
            'module' => 'tuner',
            'task' => 'delete-track',
            'track-id' => $track->getId(),
        ]);
        $jsDeleteWarning = "return confirm('Delete " . htmlspecialchars($track->getName()) . ". Are you sure?');";
        $artistLink = \SRouter::route([
            'module' => 'tuner',
            'template' => 'edit-artist',
            'artist-id' => $track->getArtist()->getId(),
        ]);
        ?>
        <tr>
            <td><?= htmlspecialchars($track->getName()); ?></td>
            <td>
                <a href="<?= htmlspecialchars($artistLink); ?>"><?= htmlspecialchars($track->getArtist()->getPublicName()); ?></a>
            </td>
            <td><?= htmlspecialchars($track->getComment()); ?></td>
            <td><a href="<?= htmlspecialchars($editLink); ?>">edit</a></td>
            <td><a href="<?= htmlspecialchars($deleteLink); ?>" onclick="<?= $jsDeleteWarning; ?>">delete</a></td>
        </tr>
    <?php endforeach; ?>
</table>
<?php endif; ?>