<?php

namespace App\Modules\Tuner\Controllers;

use App\Modules\Tuner\Repositories\TunerRepository;

/**
 * Class BaseController
 *
 * @package App\Modules\Tuner\Controllers
 */
class BaseController extends \Core\Controller\ControllerAbstract
{
    const TRACK = 'track';
    const ARTIST = 'artist';
    const SUPPLIER = 'supplier';

    /**
     * Default view name is "tracks"
     *
     * @var string
     */
    protected $defaultTemplate = 'tracks';

    /**
     * Task for saving track (create and update)
     */
    public function saveTrackTask()
    {
        $trackId      = (int)\SRequester::getPostParam('track_id');
        $trackName    = \SRequester::getPostParam('track_name');
        $artistName   = \SRequester::getPostParam('track_artist');
        $trackComment = \SRequester::getPostParam('track_comment');
        $addNew   = \SRequester::getPostParam('add_new');
        $editMore = \SRequester::getPostParam('edit_more');

        if (!$trackName || !$artistName) {
            \SReporter::addMessage('Marked (*) fields are required', \SReporter::TYPE_WARNING);
            $urlParts = ['module' => 'tuner', 'template' => 'edit-track',];
            if ($trackId) {
                $urlParts['track-id'] = $trackId;
            }
            \SRouter::redirect($urlParts);
        } else {
            $repository = TunerRepository::getInstance();

            $artists = $repository->findArtistsByPublicName($artistName);
            if (!$artists) {
                $artist = $repository->saveArtistWithData(null, $artistName);
            } elseif (count($artists) == 1) {
                $artist = current($artists);
            } else {
                \SLogger::writeLog('Artist duplicate: "' . $artistName . '"');
            }

            $result = false;
            if (!empty($artist) && !empty($artist->getId())) {
                $result = $repository->saveTrackWithData($trackId, $trackName, $artist->getId(), $trackComment);
            }
            if ($result) {
                \SReporter::addMessage('Track saved', \SReporter::TYPE_INFO);
            } else {
                \SReporter::addMessage(
                    'Track wasn\'t saved. Something went wrong',
                    \SReporter::TYPE_ERROR
                );
            }
        }

        $urlParts = ['module' => 'tuner', 'template' => 'tracks'];
        if ($addNew) {
            $urlParts['template'] = 'edit-track';
        } elseif ($editMore && !empty($result) && $result->getId()) {
            $urlParts['template'] = 'edit-track';
            $urlParts['track-id'] = $result->getId();
        }

        \SRouter::redirect($urlParts);
    }

    /**
     * Task for saving artist (create and update)
     */
    public function saveArtistTask()
    {
        $artistId   = (int)\SRequester::getPostParam('artist_id');
        $artistName = \SRequester::getPostParam('artist_public_name');
        $artistDescription = \SRequester::getPostParam('artist_description');
        $addNew   = \SRequester::getPostParam('add_new');
        $editMore = \SRequester::getPostParam('edit_more');

        if (!$artistName) {
            \SReporter::addMessage('Marked (*) fields are required', \SReporter::TYPE_WARNING);
            $urlParts = ['module' => 'tuner', 'template' => 'edit-artist',];
            if ($artistId) {
                $urlParts['artist-id'] = $artistId;
            }
            \SRouter::redirect($urlParts);
        } else {
            $result = TunerRepository::getInstance()
                ->saveArtistWithData($artistId, $artistName, $artistDescription);

            if ($result) {
                \SReporter::addMessage('Artist saved', \SReporter::TYPE_INFO);
            } else {
                \SReporter::addMessage(
                    'Artist wasn\'t saved. Something went wrong',
                    \SReporter::TYPE_ERROR
                );
            }
        }

        $urlParts = ['module' => 'tuner', 'template' => 'artists'];
        if ($addNew) {
            $urlParts['template'] = 'edit-artist';
        } elseif ($editMore && !empty($result) && $result->getId()) {
            $urlParts['template'] = 'edit-artist';
            $urlParts['artist-id'] = $result->getId();
        }

        \SRouter::redirect($urlParts);
    }

    /**
     * Task for saving music supplier (create and update)
     */
    public function saveSupplierTask()
    {
        $supplierId   = (int)\SRequester::getPostParam('supplier_id');
        $supplierName = \SRequester::getPostParam('supplier_name');
        $supplierHomeLink = \SRequester::getPostParam('supplier_home_link');
        $addNew   = \SRequester::getPostParam('add_new');
        $editMore = \SRequester::getPostParam('edit_more');

        if (!$supplierName || !$supplierHomeLink) {
            \SReporter::addMessage('Marked (*) fields are required', \SReporter::TYPE_WARNING);
            $urlParts = ['module' => 'tuner', 'template' => 'edit-supplier',];
            if ($supplierId) {
                $urlParts['supplier-id'] = $supplierId;
            }
            \SRouter::redirect($urlParts);
        } else {
            $result = TunerRepository::getInstance()
                ->saveSupplierWithData($supplierId, $supplierName, $supplierHomeLink);

            if ($result) {
                \SReporter::addMessage('Music supplier saved', \SReporter::TYPE_INFO);
            } else {
                \SReporter::addMessage(
                    'Music supplier wasn\'t saved. Something went wrong',
                    \SReporter::TYPE_ERROR
                );
            }
        }

        $urlParts = ['module' => 'tuner', 'template' => 'suppliers'];
        if ($addNew) {
            $urlParts['template'] = 'edit-supplier';
        } elseif ($editMore && !empty($result) && $result->getId()) {
            $urlParts['template'] = 'edit-supplier';
            $urlParts['supplier-id'] = $result->getId();
        }

        \SRouter::redirect($urlParts);
    }

    /**
     * Deletes track, artist or music supplier
     *
     * @param int    $id         Entity id
     * @param string $entityType String "track", "artist" or "supplier"
     *
     * @return bool
     */
    protected function deleteEntity($id, $entityType)
    {
        switch ($entityType) {
            case self::TRACK:
                $entityInMessage = 'Track';
                $entityMethod = 'deleteTrack';
                break;
            case self::ARTIST:
                $entityInMessage = 'Artist';
                $entityMethod = 'deleteArtist';
                break;
            case self::SUPPLIER:
                $entityInMessage = 'Supplier';
                $entityMethod = 'deleteSupplier';
                break;
            default:
                \SReporter::addMessage('Trying to non-determined entity', \SReporter::TYPE_ERROR);
                return false;
        }

        $result = false;
        if (!$id) {
            \SReporter::addMessage('Id is missing', \SReporter::TYPE_WARNING);
        } else {
            $result = TunerRepository::getInstance()
                ->$entityMethod($id);
            if ($result) {
                \SReporter::addMessage($entityInMessage . '\'s been removed', \SReporter::TYPE_INFO);
            } else {
                \SReporter::addMessage(
                    $entityInMessage . ' wasn\'t removed. Something went wrong',
                    \SReporter::TYPE_WARNING
                );
            }
        }

        return $result;
    }

    /**
     * Task for deleting track
     */
    public function deleteTrackTask()
    {
        $trackId = \SRequester::getGetParam('track-id');

        $this->deleteEntity($trackId, self::TRACK);

        \SRouter::redirect(['module' => 'tuner',]);
    }

    /**
     * Task for deleting artist
     */
    public function deleteArtistTask()
    {
        $artistId = \SRequester::getGetParam('artist-id');

        $result = TunerRepository::getInstance()
            ->getArtistTracksCount($artistId);

        if (!is_int($result)) {
            \SReporter::addMessage(
                'Cannot delete the artist. Something went wrong',
                \SReporter::TYPE_ERROR
            );
        } elseif ($result > 0) {
            $message = 'Artist still has ' . (int)$result . ' track' . ($result === 1 ? '' : 's') . ' stored.'
                . ' Remove them before delete this artist';
            \SReporter::addMessage(
                $message,
                \SReporter::TYPE_WARNING
            );
        } else {
            $this->deleteEntity($artistId, self::ARTIST);
        }

        \SRouter::redirect(['module' => 'tuner', 'template' => 'artists',]);
    }

    /**
     * Task for deleting music supplier
     */
    public function deleteSupplierTask()
    {
        $supplierId = \SRequester::getGetParam('supplier-id');

        $result = TunerRepository::getInstance()
            ->getSupplierTracksCount($supplierId);

        if (!is_int($result)) {
            \SReporter::addMessage(
                'Cannot delete the music supplier. Something went wrong',
                \SReporter::TYPE_ERROR
            );
        } elseif ($result > 0) {
            $message = 'Music supplier still has ' . (int)$result . ' track' . ($result === 1 ? '' : 's') . ' stored.'
                . ' Remove them before delete this music supplier';
            \SReporter::addMessage(
                $message,
                \SReporter::TYPE_WARNING
            );
        } else {
            $this->deleteEntity($supplierId, self::SUPPLIER);
        }

        \SRouter::redirect(['module' => 'tuner', 'template' => 'suppliers',]);
    }
}