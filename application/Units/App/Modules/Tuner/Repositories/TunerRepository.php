<?php

namespace App\Modules\Tuner\Repositories;

class TunerRepository extends \Core\Repository\RepositoryAbstract
{
    /**
     * Tracks table
     *
     * @var TracksTable
     */
    private $tracksTable;

    /**
     * Artist table
     *
     * @var ArtistsTable
     */
    private $artistsTable;

    /**
     * Music suppliers table
     *
     * @var MusicSuppliersTable
     */
    private $suppliersTable;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tracksTable    = new TracksTable();
        $this->artistsTable   = new ArtistsTable();
        $this->suppliersTable = new MusicSuppliersTable();
    }

    /**
     * Returns last tracks
     *
     * @param int $limit Limit number of the data set
     *
     * @return Track[]|null
     */
    public function getLastTracks($limit = 100)
    {
        $tracks = [];
        $result = $this->tracksTable->loadLastTracks($limit);
        if (!is_array($result)) {
            return null;
        }

        foreach ($result as $row) {
            $artist = new Artist(
                $row->artist_id,
                $row->artist_created_at,
                $row->artist_updated_at,
                $row->artist_public_name,
                $row->artist_other_names,
                $row->artist_description
            );
            $tracks[] = new Track(
                $row->id,
                $row->created_at,
                $row->updated_at,
                $row->name,
                $artist,
                $row->comment
            );
        }

        return $tracks;
    }

    /**
     * Returns track
     *
     * @param int $id Track id
     *
     * @return Track|null
     */
    public function getTrack($id)
    {
        $row = $this->tracksTable->loadTrack((int)$id);

        if (!$row) {
            return null;
        }

        $artist = new Artist(
            $row->artist_id,
            $row->artist_created_at,
            $row->artist_updated_at,
            $row->artist_public_name,
            $row->artist_other_names,
            $row->artist_description
        );
        $track = new Track(
            $row->id,
            $row->created_at,
            $row->updated_at,
            $row->name,
            $artist,
            $row->comment
        );

        return $track;
    }

    /**
     * Creates and saves track
     *
     * @param int    $id       Track id
     * @param string $name     Track name
     * @param int    $artistId Artist id
     * @param string $comment  Track comment
     *
     * @return Track|bool
     */
    public function saveTrackWithData($id = null, $name, $artistId, $comment = null)
    {
        if (!($artist = $this->getArtist((int)$artistId))) {
            \SLogger::writeLog('Artist doesn\'t exist, id: ' . $artistId . '. Track name: ' . $name);
            return false;
        }

        $track = new Track($id, null, null, $name, $artist, $comment);

        if ($this->saveTrack($track)) {
            return $track;
        }

        return false;
    }

    /**
     * Saves track
     *
     * @param Track $track Track to save
     *
     * @return bool
     */
    public function saveTrack(Track $track)
    {
        return $this->tracksTable->save($track);
    }

    /**
     * Returns last artists
     *
     * @param int $limit Limit number of the data set
     *
     * @return Artist[]
     */
    public function getLastArtists($limit = 100)
    {
        $artists = [];
        foreach ($this->artistsTable->loadLastArtists($limit) as $row) {
            $artists[] = new Artist(
                $row->id,
                $row->created_at,
                $row->updated_at,
                $row->public_name,
                $row->other_names,
                $row->description
            );
        }

        return $artists;
    }

    /**
     * Returns artist
     *
     * @param int $id Artist id
     *
     * @return Artist|null
     */
    public function getArtist($id)
    {
        $data = $this->artistsTable->find((int)$id);
        if (!$data) {
            return null;
        }

        $artist = new Artist(
            $data->id,
            $data->created_at,
            $data->updated_at,
            $data->public_name,
            $data->other_names,
            $data->description
        );

        return $artist;
    }

    /**
     * Returns count of the artist's tracks
     *
     * @param int $artistId Artist's id
     *
     * @return int|null
     */
    public function getArtistTracksCount($artistId)
    {
        $result = $this->tracksTable->getArtistTracksCount($artistId);

        if (!$result) {
            return null;
        }

        return (int)$result->count ?? null;
    }

    /**
     * Removes track
     *
     * @param int $id Track id
     *
     * @return bool
     */
    public function deleteTrack($id)
    {
        if (!$id) {
            return false;
        }

        return (bool)$this->tracksTable->deleteTrack($id);
    }

    /**
     * Removes artist
     *
     * @param int $id Artist id
     *
     * @return bool
     */
    public function deleteArtist($id)
    {
        if (!$id) {
            return false;
        }

        return (bool)$this->artistsTable->delete($id);
    }

    /**
     * Returns artist's id. Can add artist to db if he isn't presented in the db.
     *
     * @param string $publicName
     *
     * @return Artist[]
     */
    public function findArtistsByPublicName($publicName)
    {
        $artistsData = $this->artistsTable->findByPublicName($publicName);

        $artists = [];
        foreach ($artistsData as $data) {
            $artists[] = new Artist(
                $data->id,
                $data->created_at,
                $data->updated_at,
                $data->public_name,
                $data->other_names,
                $data->description
            );
        }

        return $artists;
    }

    /**
     * Saves artist in the db
     *
     * @param Artist $artist Artist to save
     *
     * @return bool
     */
    public function saveArtist(Artist $artist)
    {
        return $this->artistsTable->save($artist);
    }

    /**
     * Creates and saves artist in the db
     *
     * @param int    $id          Artist id
     * @param string $publicName  Public name
     * @param string $description Description
     *
     * @return Artist|bool
     */
    public function saveArtistWithData($id = null, $publicName, $description = null)
    {
        $artist = new Artist($id, null, null, $publicName, null, $description);

        if ($this->saveArtist($artist)) {
            return $artist;
        }

        return false;
    }

    /**
     * Returns all music suppliers
     *
     * @return array|null
     */
    public function getAllSuppliers()
    {
        $suppliers = [];
        $result = $this->suppliersTable->loadAllSuppliers();
        if (!is_array($result)) {
            return null;
        }

        foreach ($result as $row) {
            $suppliers[] = new MusicSupplier(
                $row->id,
                $row->created_at,
                $row->updated_at,
                $row->name,
                $row->home_link
            );
        }

        return $suppliers;
    }

    /**
     * Creates and saves supplier in the db
     *
     * @param int    $id       Id
     * @param string $name     Name
     * @param string $homeLink Home link
     *
     * @return MusicSupplier|bool
     */
    public function saveSupplierWithData($id = null, $name, $homeLink = null)
    {
        $supplier = new MusicSupplier($id, null, null, $name, $homeLink);

        if ($this->saveSupplier($supplier)) {
            return $supplier;
        }

        return false;
    }

    /**
     * Saves supplier in the db
     *
     * @param MusicSupplier $supplier Supplier to save
     *
     * @return bool
     */
    public function saveSupplier(MusicSupplier $supplier)
    {
        return $this->suppliersTable->save($supplier);
    }

    /**
     * Returns supplier
     *
     * @param int $id Supplier id
     *
     * @return MusicSupplier|null
     */
    public function getSupplier($id)
    {
        $data = $this->suppliersTable->find((int)$id);
        if (!$data) {
            return null;
        }

        $artist = new MusicSupplier(
            $data->id,
            $data->created_at,
            $data->updated_at,
            $data->name,
            $data->home_link
        );

        return $artist;
    }

    /**
     * Returns count of the music supplier's tracks
     *
     * @param int $supplierId Music supplier id
     *
     * @return int|null
     */
    public function getSupplierTracksCount($supplierId)
    {
        $result = $this->tracksTable->getSupplierTracksCount($supplierId);

        if (!$result) {
            return null;
        }

        return (int)$result->count ?? null;
    }

    /**
     * Removes supplier
     *
     * @param int $id Supplier id
     *
     * @return bool
     */
    public function deleteSupplier($id)
    {
        if (!$id) {
            return false;
        }

        return (bool)$this->suppliersTable->delete($id);
    }

}