<?php

namespace App\Modules\Tuner\Repositories;

class TracksTable extends \Core\Repository\TableAbstract
{
    protected $tableName = 'tracks';

    /**
     * Loads last tracks
     *
     * @param int $limit Limit number of the result set
     *
     * @return \stdClass[]|null
     */
    public function loadLastTracks($limit = 100)
    {
        $db = \SFactory::getDb();

        $tracksTbl  = $db->qn($this->getTableName());
        $bridgeTbl  = $db->qn(\Constants\DbTables::TRACKS_ARTISTS_BRIDGE);
        $artistsTbl = $db->qn(\Constants\DbTables::ARTISTS);

        $select = [
            $tracksTbl . '.* ',
            $artistsTbl . '.id AS artist_id',
            $artistsTbl . '.created_at AS artist_created_at',
            $artistsTbl . '.updated_at AS artist_updated_at',
            $artistsTbl . '.public_name AS artist_public_name',
            $artistsTbl . '.other_names AS artist_other_names',
            $artistsTbl . '.description AS artist_description',
        ];

        $query = $db->getQuery()
            ->select($select)
            ->from($tracksTbl, true, false)
            ->leftJoin($bridgeTbl, $bridgeTbl . '.`track_id` = ' . $tracksTbl . '.`id`')
            ->leftJoin($artistsTbl, $bridgeTbl . '.`artist_id` = ' . $artistsTbl . '.`id`')
            ->orderBy($tracksTbl . '.`created_at`', 'DESC', false, false);

        if (ctype_digit((string)$limit)) {
            $query->limit(0, $limit);
        }

        if (!$result = $db->execute($query)) {
            \SLogger::writeLog('SQL error in getLastTracks. ' . $query->buildQuery());
            return null;
        }

        return $result->fetchAllObj();
    }

    /**
     * Loads track with all dependencies
     *
     * @param int $id Track id
     *
     * @return \stdClass
     */
    public function loadTrack($id)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $tracksTbl  = $db->qn($this->getTableName());
        $bridgeTbl  = $db->qn(\Constants\DbTables::TRACKS_ARTISTS_BRIDGE);
        $artistsTbl = $db->qn(\Constants\DbTables::ARTISTS);

        $select = [
            $tracksTbl . '.* ',
            $artistsTbl . '.id AS artist_id',
            $artistsTbl . '.created_at AS artist_created_at',
            $artistsTbl . '.updated_at AS artist_updated_at',
            $artistsTbl . '.public_name AS artist_public_name',
            $artistsTbl . '.other_names AS artist_other_names',
            $artistsTbl . '.description AS artist_description',
        ];

        $artistsJoin = $bridgeTbl . '.' . $db->qn('artist_id') . ' = ' . $artistsTbl . '.' . $db->qn('id');

        $query->select($select)
            ->from($tracksTbl, true, false)
            ->leftJoin($bridgeTbl, $bridgeTbl . '.' . $db->qn('track_id') . ' = ' . (int)$id)
            ->leftJoin($artistsTbl, $artistsJoin)
            ->where($tracksTbl . '.`id` = ' . (int)$id);

        if (!$result = $db->execute($query)) {
            return null;
        }

        return $result->fetchRowObj();
    }

    /**
     * Saves track data
     *
     * @param Track $track Track object
     *
     * @return bool
     */
    public function save(Track $track)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $data = [
            'name'    => $track->getName(),
            'comment' => $track->getComment(),
        ];

        if ($track->getId()) {
            $query->update($this->tableName, $data)
                ->where('id = ' . (int)$track->getId());
            $result = $db->execute($query);
        } else {
            //TODO: start transaction
            $query->insert($this->tableName, $data);
            $result = $db->execute($query);

            $driver = $db->getDriver();
            if (
                $result
                && method_exists($driver, 'getLastInsertedId')
                && ($newId = $driver->getLastInsertedId())
            ) {
                $track->setId($newId);
            }

            if (empty($newId)) {
                //TODO: revert transaction
                return false;
            }
            $data = [
                'track_id'  => $newId,
                'artist_id' => $track->getArtist()->getId(),
            ];
            $query->insert(\Constants\DbTables::TRACKS_ARTISTS_BRIDGE, $data);

            $result = $result & $db->execute($query);
            if (!$result) {
                //TODO: revert transaction
                return false;
            }
            //TODO: end transaction
        }

        return (bool)$result;
    }

    public function deleteTrack($id)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        //TODO: start transaction
        $query->delete()
            ->from(\Constants\DbTables::TRACKS_ARTISTS_BRIDGE)
            ->where('track_id = ' . (int)$id);

        if (!($result = $db->execute($query))) {
            //TODO: revert transaction
            return false;
        }

        $query->delete()
            ->from($this->getTableName())
            ->where('id = ' . (int)$id);
        //TODO: end transaction

        if (!($result = $result & $db->execute($query))) {
            //TODO: revert transaction
            return false;
        }

        return (bool)$result;
    }

    /**
     * Returns count of the artist's tracks
     *
     * @param int $artistId Artist id
     *
     * @return null|\stdClass
     */
    public function getArtistTracksCount($artistId)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $query->select('COUNT(DISTINCT track_id) as count')
            ->from(\Constants\DbTables::TRACKS_ARTISTS_BRIDGE)
            ->where($db->qn('artist_id') . ' = ' . (int)$artistId);

        if (!$result = $db->execute($query)) {
            return null;
        }

        return $result->fetchRowObj();
    }

    /**
     * Returns count of the music supplier tracks
     *
     * @param int $supplierId Music supplier id
     *
     * @return null|\stdClass
     */
    public function getSupplierTracksCount($supplierId)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $query->select('COUNT(DISTINCT track_id) as count')
            ->from(\Constants\DbTables::MUSIC_SUPPLIERS_TRACKS_BRIDGE)
            ->where($db->qn('music_supplier_id') . ' = ' . (int)$supplierId);

        if (!$result = $db->execute($query)) {
            return null;
        }

        return $result->fetchRowObj();
    }
}