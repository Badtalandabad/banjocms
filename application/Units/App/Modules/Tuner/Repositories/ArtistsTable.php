<?php

namespace App\Modules\Tuner\Repositories;

/**
 * Artists db table
 *
 * @package App\Modules\Tuner\Repositories
 */
class ArtistsTable extends \Core\Repository\TableAbstract
{
    protected $tableName = 'artists';

    public function save(Artist $artist)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $data = ['public_name' => $artist->getPublicName()];

        if ($artist->getOtherNames()) {
            $data['other_names'] = $artist->getOtherNames();
        }
        if ($artist->getDescription()) {
            $data['description'] = $artist->getDescription();
        }

        if ($artist->getId()) {
            $query->update($this->tableName, $data)
                ->where('id = ' . (int)$artist->getId());
        } else {
            $query->insert($this->tableName, $data);
        }

        $result = $db->execute($query);

        $driver = $db->getDriver();
        if (
            $result
            && method_exists($driver, 'getLastInsertedId')
            && ($newId = $driver->getLastInsertedId())
        ) {
            $artist->setId($newId);
        }

        return (bool)$result;
    }

    public function findByPublicName($publicName, $orderBy = 'created_at', $sorting = 'DESC')
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $query->select()
            ->from($this->getTableName())
            ->where($db->qn('public_name') . ' LIKE ' . $db->q($publicName))
            ->orderBy($orderBy, $sorting);

        $result = $db->execute($query);
        if (!$result) {
            return null;
        }

        return $result->fetchAllObj();
    }

    /**
     * Loads last artists
     *
     * @param int $limit Limit number of the result set
     *
     * @return \stdClass[]
     */
    public function loadLastArtists($limit = 100)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery()
            ->select()
            ->from($this->tableName)
            ->orderBy('created_at', 'DESC');

        if (ctype_digit((string)$limit)) {
            $query->limit(0, $limit);
        }

        if (!$result = $db->execute($query)) {
            return [];
        }

        return $result->fetchAllObj();
    }

}