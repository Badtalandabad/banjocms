<?php

namespace App\Modules\Tuner\Repositories;

/**
 * Track entity
 */
class Track extends \Core\Repository\EntityAbstract
{
    /**
     * Track name
     *
     * @var string
     */
    protected $name;

    /**
     * Artist of the track
     *
     * @var Artist
     */
    protected $artist;

    /**
     * Music suppliers
     *
     * @var MusicSupplier[]
     */
    protected $musicSuppliers = [];

    /**
     * Track comment
     *
     * @var string
     */
    protected $comment;

    /**
     * Track constructor.
     *
     * @param int              $id        Track id
     * @param \DateTime|string $createdAt Creation time
     * @param \DateTime|string $updatedAt Last update time
     * @param string           $name      Track name
     * @param Artist           $artist    Artist of the track
     * @param MusicSupplier[]  $suppliers Music suppliers
     * @param string           $comment   Track comment
     */
    public function __construct(
        $id = null,
        $createdAt = null,
        $updatedAt = null,
        $name,
        $artist = null,
        array $suppliers = null,
        $comment = null
    ) {
        parent::__construct($id, $createdAt, $updatedAt);

        $this->setName($name)
            ->setArtist($artist)
            ->setMusicSuppliers($suppliers)
            ->setComment($comment);
    }

    /**
     * Returns track name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets track name
     *
     * @param string $name Track name
     *
     * @return self
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Returns artist of the track
     *
     * @return Artist
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Sets artist of the track
     *
     * @param Artist $artist Artist of the track
     *
     * @return self
     */
    public function setArtist(Artist $artist = null): self
    {
        if (!$artist) {
            $artist = new Artist(null, null, null, null, null, null);
        }
        $this->artist = $artist;
        return $this;
    }

    /**
     * Returns track comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Sets track comment
     *
     * @param string $comment
     *
     * @return self
     */
    public function setComment($comment): self
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * Returns list of music suppliers
     *
     * @return MusicSupplier[]
     */
    public function getMusicSuppliers()
    {
        return $this->musicSuppliers;
    }

    /**
     * Sets the list of music suppliers
     *
     * @param MusicSupplier[] $musicSuppliers
     *
     * @return $this
     */
    public function setMusicSuppliers(array $musicSuppliers = null) : self
    {
        if (is_null($musicSuppliers)) {
            $musicSuppliers = [];
        }
        $this->musicSuppliers = $musicSuppliers;

        return $this;
    }

    /**
     * Adds music supplier to the store
     *
     * @param MusicSupplier $musicSupplier Music supplier
     *
     * @return $this
     */
    public function addMusicSupplier(MusicSupplier $musicSupplier) : self
    {
        $this->musicSuppliers[] = $musicSupplier;

        return $this;
    }
}