<?php

namespace App\Modules\Tuner\Repositories;

/**
 * Music suppliers db table
 *
 * @package App\Modules\Tuner\Repositories
 */
class MusicSuppliersTable extends \Core\Repository\TableAbstract
{
    protected $tableName = 'music_suppliers';

    /**
     * Loads all suppliers
     *
     * @return \stdClass[]|null
     */
    public function loadAllSuppliers()
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery()
            ->select()
            ->from($this->getTableName())
            ->orderBy('name', 'DESC');

        if (!$result = $db->execute($query)) {
            return null;
        }

        return $result->fetchAllObj();
    }

    /**
     * Saves music supplier in the db
     *
     * @param MusicSupplier $supplier
     *
     * @return bool
     */
    public function save(MusicSupplier $supplier)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $data = ['name' => $supplier->getName()];

        if ($supplier->getHomeLink()) {
            $data['home_link'] = $supplier->getHomeLink();
        }

        if ($supplier->getId()) {
            $query->update($this->getTableName(), $data)
                ->where('id = ' . (int)$supplier->getId());
        } else {
            $query->insert($this->getTableName(), $data);
        }

        $result = $db->execute($query);

        $driver = $db->getDriver();
        if (
            $result
            && method_exists($driver, 'getLastInsertedId')
            && ($newId = $driver->getLastInsertedId())
        ) {
            $supplier->setId($newId);
        }

        return (bool)$result;
    }

}