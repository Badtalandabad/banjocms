<?php

namespace App\Modules\Tuner\Repositories;

/**
 * Artist entity
 */
class Artist extends \Core\Repository\EntityAbstract
{
    /**
     * Artist's public name
     *
     * @var string
     */
    protected $publicName;

    /**
     * Other artist's names
     *
     * @var string
     */
    protected $otherNames;

    /**
     * Description
     *
     * @var string
     */
    protected $description;

    /**
     * Artist constructor.
     *
     * @param int       $id          Id
     * @param \DateTime $createdAt   Creation time
     * @param \DateTime $updatedAt   Last update time
     * @param string    $publicName  Public name
     * @param string    $otherNames  Other artist's names
     * @param string    $description Description
     *
     * @thrown \Core\Exception\NonFatalException If wrong id or creation/update time were given
     */
    public function __construct($id, $createdAt, $updatedAt, $publicName, $otherNames, $description)
    {
        parent::__construct($id, $createdAt, $updatedAt);

        $this->setPublicName($publicName);
        $this->setOtherNames($otherNames);
        $this->setDescription($description);
    }

    /**
     * Returns public name
     *
     * @return string
     */
    public function getPublicName()
    {
        return $this->publicName;
    }

    /**
     * Sets public name
     *
     * @param string $publicName
     *
     * @return self
     */
    public function setPublicName($publicName): self
    {
        $this->publicName = $publicName;
        return $this;
    }

    /**
     * Returns other artist's names
     *
     * @return string
     */
    public function getOtherNames()
    {
        return $this->otherNames;
    }

    /**
     * Sets other artists names
     *
     * @param string $otherNames
     *
     * @return self
     */
    public function setOtherNames($otherNames): self
    {
        $this->otherNames = $otherNames;
        return $this;
    }

    /**
     * Returns description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets description
     *
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description): self
    {
        $this->description = $description;
        return $this;
    }

}