<?php

namespace App\Modules\Tuner\Repositories;

/**
 * Music supplier entity
 */
class MusicSupplier extends \Core\Repository\EntityAbstract
{
    /**
     * Supplier's name
     *
     * @var string
     */
    protected $name;

    /**
     * Home link
     *
     * @var string
     */
    protected $homeLink;

    /**
     * Supplier constructor.
     *
     * @param int       $id          Id
     * @param \DateTime $createdAt   Creation time
     * @param \DateTime $updatedAt   Last update time
     * @param string    $name        Name
     * @param string    $homeLink    Home link
     *
     * @thrown \Core\Exception\NonFatalException If wrong id or creation/update time were given
     */
    public function __construct($id, $createdAt, $updatedAt, $name, $homeLink)
    {
        parent::__construct($id, $createdAt, $updatedAt);

        $this->setName($name);
        $this->setHomeLink($homeLink);
    }

    /**
     * Returns name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Returns home link
     *
     * @return string
     */
    public function getHomeLink()
    {
        return $this->homeLink;
    }

    /**
     * Sets home link
     *
     * @param string $homeLink
     *
     * @return self
     */
    public function setHomeLink($homeLink): self
    {
        $this->homeLink = $homeLink;
        return $this;
    }

}