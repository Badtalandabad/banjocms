<?php

namespace App\Modules\Search\Library;

use Core\Curl\Curl;

/**
 * Abstract declare of parser engine.
 *
 * @package Parser
 */
abstract class ParserAbstract
{
    /**
     * cURL object
     *
     * @var Curl
     */
    protected $_curl;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_curl = new Curl();
    }

    /**
     * Connects to host, downloads page, and returns it
     *
     * @param   string  $host   Link to the page, which will be downloaded
     *
     * @return  string
     */
    protected function _downloadPage($host)
    {
        $this->_curl->connectToHost($host);

        $page = $this->_curl->execute();

        $this->_curl->disconnect();

        return $page;
    }

    /**
     * Returns last connected host
     *
     * @return string
     */
    protected function _getLastHost()
    {
        $lastHost = $this->_curl->getLastHost();

        return $lastHost;
    }

    /**
     * Changes request method on POST, curl option will be set for
     * every following connection
     */
    protected function _setPostMethod()
    {
        $this->_curl->setOptionPermanent(CURLOPT_POST, true);
    }

    /**
     * Sets requests parameters for POST method
     *
     * @param   array   $parameters Array with the parameters, keys are names of its
     *
     * @throws  ParserException If function argument is wrong
     */
    protected function _setPostRequestParameters($parameters)
    {
        if ((!is_array($parameters)) || count($parameters)) {
            $exceptionMessage = 'Wrong cURL post parameters. It must be an array.';
            throw new ParserException($exceptionMessage);
        }

        $stringParameters = '';
        foreach ($parameters as $name => $value) {
            $stringParameters .= $name . '=' . $value . '&';
        }

        //deletes surplus ampersand
        $stringParameters = substr($stringParameters, 0, -1);

        $this->_curl->setOptionOnce(CURLOPT_POSTFIELDS, $stringParameters);
    }

}
