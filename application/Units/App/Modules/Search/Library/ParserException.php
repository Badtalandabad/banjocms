<?php

namespace App\Modules\Search\Library;

use Core\Exception\NonFatalException;

/**
 * Parser exception
 *
 * @package Exception\Parser
 */
class ParserException extends NonFatalException
{
    /**
     * Initialisation
     */
    public function init()
    {
        $this->message = 'Parser exception: ' . $this->message;
    }
}
