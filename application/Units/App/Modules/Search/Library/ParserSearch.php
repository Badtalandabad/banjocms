<?php

namespace App\Modules\Search\Library;

/**
 * Class ParserSearch
 * Parser gets Google search issue links for a certain request,
 * and downloads all the pages from the first issue page. No more than limit.
 *
 * @package Parser
 */
class ParserSearch extends ParserAbstract
{
    const UTF8 = 'UTF-8';
    const ANSI = 'ANSI';

    /**
     * Limit of the pages from search issue to parse.
     *
     * @var int
     */
    private $_linksLimit = 10;

    /**
     * Links from the search issue.
     *
     * @var array
     */
    private $_issueLinks = array();

    /**
     * Downloaded pages from search issue.
     *
     * @var array
     */
    private $_issuePages = array();

    /**
     * Saves issue link in the _issueLinks array
     *
     * @param   string  $link   Link to the some site
     */
    private function _addIssueLink($link)
    {
        $this->_issueLinks[] = $link;
    }

    /**
     * Saves page in the _issuePages array
     *
     * @param   string  $link   Link to the page
     * @param   string  $page   Page's source code
     */
    private function _saveIssuePage($link, $page)
    {
        $this->_issuePages[$link] = $page;
    }

    /**
     * Downloads google search issue, parses it and returns links to the pages
     *
     * @param   string  $searchRequest  Search request from user
     *
     * @return  array
     */
    public function parseGoogleIssueLinks( $searchRequest )
    {
        $googleLink = 'https://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=8&q='
            . urlencode($searchRequest);

        $jsonData = $this->_downloadPage($googleLink);

        $data = json_decode($jsonData);

        if ($data->responseData) {
            foreach ($data->responseData->results as $resultUnit) {
                $this->_addIssueLink($resultUnit->unescapedUrl);
            }
        }

        return $this->_issueLinks;
    }

    /**
     * Downloads and returns pages from the google search issue
     *
     * @param   string|null $resultEncoding Encoding to save page, if null given
     *                                      saves everything as it is
     *
     * @return  array
     */
    public function parseGoogleIssueHosts($resultEncoding = null)
    {
        foreach ($this->_issueLinks as $issueLink) {
            $page = $this->_downloadPage($issueLink);

            if ( $resultEncoding ) {
                $regExp = '/<meta.*charset="?(.+);?".*>/';
                $matches = array();
                $pageEncoding = false;
                if ( preg_match( $regExp, $page, $matches ) ) {
                    $pageEncoding = $matches[1];
                }

                if ( ! $pageEncoding ) {
                    continue;
                }

                $page = iconv($pageEncoding, $resultEncoding, $page);
            }

            $this->_saveIssuePage($issueLink, $page);
        }

        return $this->_issuePages;
    }

}