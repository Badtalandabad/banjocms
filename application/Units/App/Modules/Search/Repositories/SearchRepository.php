<?php

namespace App\Modules\Search\Repositories;

/**
 * Class SearchRepository
 * Contains methods for data operations with the search issue pages
 */
class SearchRepository extends \Core\Repository\RepositoryAbstract
{
    private $tableName = 'google_search_issue';

    /**
     * Saves all pages, truncates table before saving
     *
     * @param   array   $data   Array with pages
     */
    public function savePages($data)
    {
        $db = \SFactory::getDb();
        $tableName = $db->qn($this->tableName);

        //$this->_createTable();

        $query = $db->getQuery()
            ->truncate($tableName);

        $db->execute($query);

        foreach ($data as $link => $page) {
            $sets = array(
                'link' => $link,
                'page' => $page,
            );
            //$sets = $db->tidySets( $sets );

            $query = $db->getQuery()
                ->insert($tableName, $sets);

            $db->execute($query);
        }
    }

    /**
     * Gets and returns all pages
     *
     * @return  array
     */
    public function getPages()
    {
        $db = \SFactory::getDb();

        $query = $db->getQuery();

        $qColumns = $db->quoteColumns(array('id', 'link'));

        $query->select($qColumns)
            ->from($db->qn($this->tableName));

        $queryResult = $db->execute($query);

        $result = array();

        if ($queryResult) {
            while ($object = $queryResult->fetchRowObj()) {
                $result[] = $object;
            }
        }

        return $result;
    }

    /**
     * Gets and returns one page from database by its id
     *
     * @param   int $id Id of the page
     *
     * @throws  \Core\Db\DbException       if some db error occurs
     * @throws  \Core\Exception\NonFatalException    if no page was found
     *
     * @return  object
     */
    public function getPage( $id )
    {
        $db = \SFactory::getDb();

        $query = $db->getQuery()
            ->select()
            ->from($db->qn($this->tableName))
            ->where('id=' . (int)$id);

        $queryResult = $db->execute($query);

        $page = $queryResult->fetchRowObj();

        if ($queryResult->getErrorMessage()) {
            throw new \Core\Db\DbException($queryResult->getErrorMessage());
        }

        if ($page === null) {
            $exceptionMessage = 'Route error. Page with such id does not exist';
            throw new \Core\Exception\NonFatalException($exceptionMessage);
        }

        return $page;
    }

    /**
     * Saves page in the database
     *
     * @param   int     $id     ID of the page in the system storage
     * @param   string  $link   URL
     * @param   string  $page   Source code of the page
     *
     * @throws  \Core\Db\DbException   if some db error occurs
     */
    public function savePage($id, $link, $page)
    {
        $db = \SFactory::getDb();

        $sets = array(
            'link' => $link,
            'page' => $page,
        );
        //$sets = $db->tidySets( $sets );

        $query = $db->getQuery()
            ->update($db->qn($this->tableName), $sets)
            ->where('id=' . (int)$id);

        $queryResult = $db->execute($query);

        if (!$queryResult->getAffectedRowsCount()) {
            $exceptionMessage = 'Page was not saved! It seems there is no page with such id '
                . 'or another issue.';
            throw new \Core\Db\DbException($exceptionMessage);
        }

        if ( $queryResult->getErrorMessage() ) {
            $exceptionMessage = 'Page was not saved!<br />'
                . $queryResult->getErrorMessage();
            throw new \Core\Db\DbException($exceptionMessage);
        }
    }

    /**
     * Creates table, if it does not exist
     *
     * @deprecated
     */
    private function _createTable()
    {
        $db = \SFactory::getDb();

        $query = 'CREATE TABLE IF NOT EXISTS `' . $this->getPrefixedTableName() . '` ('
            . '`id` int(11) NOT NULL AUTO_INCREMENT,'
            . '`link` varchar(255) NOT NULL,'
            . '`page` text NOT NULL,'
            . 'PRIMARY KEY (`id`)'
            . ') ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;';

        $db->execute( $query );
    }

}