<?php
$this->document->setTitle('Page editing');
$action = \SRouter::route([
    'module' => 'search',
    'controller' => 'search',
    'task' => 'edit-issue-page',
]);
?>
<form action="<?php echo $action; ?>" method="post">
    <label for="link">Link:</label>
    <br />
    <input type="text" name="link" size="140" value="<?php echo htmlspecialchars($this->link); ?>" />
    <br />
    <label for="page">Page source code</label>
    <br />
    <textarea name="page" rows="37" cols="140" ><?php echo htmlspecialchars($this->page); ?></textarea>
    <br />
    <button>Save</button>
    <input type="hidden" name="id" value="<?php echo $this->id; ?>" />
</form>
