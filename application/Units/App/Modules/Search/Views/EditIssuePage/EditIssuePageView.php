<?php

namespace Modules\Search\Views\EditIssuePage;

use App\Modules\Search\Repositories\SearchRepository;

/**
 * Class EditIssuePageView
 * View of editing one page
 *
 * @package View
 */
class EditIssuePageView extends \Core\View\ViewAbstract
{
    public $id;
    public $link;
    public $page;

    /**
     * Prepares variables for the template
     */
    protected function prepareTemplate()
    {
        $id = \SRequester::getGetParam('page-id');
        $this->id = $id;

        $isRestoreNeeded = \SRequester::getSessionVariable('isRestoreNeeded');

        if ( $isRestoreNeeded ) {
            $restoreDate = \SRequester::getSessionVariable('restoreData');
            \SRequester::deleteSessionVariable('isRestoreNeeded');
            \SRequester::deleteSessionVariable('restoreData');
            $this->link = $restoreDate['link'];
            $this->page = $restoreDate['page'];
        }

        try {
            $repository = SearchRepository::getInstance();
            $page = $repository->getPage($id);
            $this->link = $page->link;
            $this->page = $page->page;
        } catch (\Core\Exception\NonFatalException $e) {
            $warningMessage = $e->getMessage();
            \SReporter::addMessage($warningMessage, \SReporter::TYPE_WARNING);

            $urlParams = array(
                'module' => 'search',
                'view' => 'search',
                'template' => 'searchissue',
            );
            \SRouter::redirect($urlParams);
        }
    }
}