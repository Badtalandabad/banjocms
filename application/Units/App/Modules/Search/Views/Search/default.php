<?php
/** @var \Core\View\Document $document */
$this->document->setTitle('Search');
$action = \SRouter::route(['module' => 'search', 'controller' => 'search', 'task' => 'search',]);
?>
<form action="<?php echo $action; ?>" method="post">
    <input type="text" name="question" size="40" />
    <button>Search</button>
</form>
