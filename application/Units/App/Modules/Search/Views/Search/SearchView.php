<?php

namespace App\Modules\Search\Views\Search;

use App\Modules\Search\Repositories\SearchRepository;

/**
 * Class SearchView
 * View of the search form and search issue operations
 *
 * @package View
 */
class SearchView extends \Core\View\ViewAbstract
{
    public $pages;

    /**
     * Prepares variables for the template
     */
    protected function prepareTemplate()
    {
        if ($this->getTemplate() == 'searchissue') {
            $this->pages = SearchRepository::getInstance()->getPages();
        }
    }

}