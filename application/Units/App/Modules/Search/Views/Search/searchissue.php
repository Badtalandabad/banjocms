<?php

$this->document->setTitle('Search issue');

if (empty($this->pages)):
?>
    There is no pages in database.
<?php else: ?>
    <table style="margin-top: 10px;">
        <?php foreach ($this->pages as $page): ?>
            <?php
            $param = array(
                'module' => 'search',
                'view' => 'editIssuePage',
                'page-id' => (int)$page->id
            );
            $href = \SRouter::route($param);
            ?>
            <tr>
                <td>
                    <?php echo $page->link; ?>
                </td>
                <td><a href="<?php echo $href; ?>">Edit</a></td>
                <td>
                    <a href="<?php echo $page->link; ?>" target="_blank">Show link</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
