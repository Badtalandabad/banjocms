<?php

namespace App\Modules\Search\Controllers;

use App\Modules\Search\Repositories\SearchRepository;
use App\Modules\Search\Library\ParserSearch;

/**
 * Class SearchController
 * Contains tasks for searching operations control
 */
class SearchController extends \Core\Controller\ControllerAbstract
{
    /**
     * Runs the search task
     * and redirects to the displaying search issue page
     */
    public function searchTask()
    {
        $moduleName = $this->getModuleName();
        $searchRequest = \SRequester::getGetParam('question');

        if ($searchRequest === '' || $searchRequest === null) {
            $message = 'Search request is empty.';
            \SReporter::addMessage($message, \SReporter::TYPE_WARNING);
            \SRouter::redirect(['module' => $moduleName, 'view' => 'search',]);
            return;
        }

        $parser = new ParserSearch();
        $parser->parseGoogleIssueLinks($searchRequest);

        $pages = $parser->parseGoogleIssueHosts(ParserSearch::UTF8);

        $repository = SearchRepository::getInstance();
        $repository->savePages($pages);

        \SRouter::redirect([
            'module'   => $moduleName,
            'view'     => 'search',
            'template' => 'searchissue',
        ]);
    }

    /**
     * Runs the editing issue page task
     * and redirects to the displaying search issue page
     *
     * @throws  \Core\Exception\SystemException  If id of the editing page is missed
     */
    public function editIssuePageTask()
    {
        $moduleName = $this->getModuleName();
        $id   = (int)\SRequester::getPostParam('id');
        $link = \SRequester::getPostParam('link');
        $page = \SRequester::getPostParam('page');

        if ($id === null) {
            $exceptionMessage = 'Route error. Id missed in editIssuePage task.';
            throw new \Core\Exception\SystemException($exceptionMessage);
        }

        $repository = SearchRepository::getInstance();
        try {
            $repository->savePage($id, $link, $page);
            \SRouter::redirect( array(
                'module'   => $moduleName,
                'view'     => 'search',
                'template' => 'searchissue' )
            );
        } catch (\Core\Exception\NonFatalException $e) {
            $errorMessage = $e->getMessage();
            \SRequester::setSessionVariable('systemError', $errorMessage);

            $restoreData = array( 'link' => $link, 'page' => $page );
            \SRequester::setSessionVariable('restoreData', $restoreData);
            \SRequester::setSessionVariable('isRestoreNeeded', true);

            $urlParams = [
                'module' => $moduleName,
                'view'   => 'editIssuePage',
                'page-id' => $id,
            ];
            \SRouter::redirect($urlParams);
        }
    }

}