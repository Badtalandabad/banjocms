<?php

namespace App\Modules\Application\Controllers;


/**
 * Class BaseController
 * Contains display task
 *
 * @package App\Modules\Application\Controllers
 */
class BaseController extends \Core\Controller\ControllerAbstract
{
    protected $defaultView = 'home';
}