<?php

namespace Admin\Library;

use Core\Auth\User;

class AdminApplication extends \Core\Application
{
    const ADMIN_PATH = 'admin';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        if (!\SRegistry::isCurrentModule('auth')
            && ($user = \SAuth::getCurrentUser())
            && $user->getRole() > User::ROLE_ADMIN
        ) {
            if (\SRegistry::isCurrentTask(DISPLAY_TASK)) {
                \SAuth::setLastUri($_SERVER['REQUEST_URI']);
            }
            \SRouter::redirect(['module' => 'auth', 'view' => 'login'], self::ADMIN_PATH);
        }
    }

}