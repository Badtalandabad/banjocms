<?php

use Admin\Library\AdminApplication;

$updateLink = \SRouter::route(['view' => 'update',], AdminApplication::ADMIN_PATH);
$registerUserLink = \SRouter::route(['view' => 'registration', 'module' => 'auth'], AdminApplication::ADMIN_PATH);
?><div>
    <ul>
        <li><a href="<?php echo $updateLink; ?>">Update</a></li>
        <li><a href="<?php echo $registerUserLink; ?>">Register new user</a></li>
    </ul>
</div>

