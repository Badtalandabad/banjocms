<?php

namespace Admin\Modules\Admin\Views\Update;

use Admin\Library\AdminViewAbstract;

/**
 * Class UpdateView
 * Update view
 */
class UpdateView extends AdminViewAbstract
{
    public $differences;

    /**
     * Prepares variables for the template
     */
    protected function prepareTemplate()
    {
        if ($this->getTemplate() == 'solve') {
            $differences = \SRequester::getSessionVariable('differences');
            $this->differences = $differences;
        }
    }
}