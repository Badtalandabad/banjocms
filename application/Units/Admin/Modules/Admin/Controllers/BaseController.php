<?php

namespace Admin\Modules\Admin\Controllers;


class BaseController extends \Core\Controller\ControllerAbstract
{
    /**
     * Updates the system
     */
    public function updateSystemTask()
    {
        $newVersion = \SUpdater::checkForUpdate();
        if ( ! $newVersion ) {
            //TODO: handle this somehow
            \SReporter::addMessage('Server says no update is needed.', \SReporter::TYPE_WARNING);
            \SRouter::redirect(array('view' => 'update',));
        }

        //Suggestion: update by token, maybe?

        $oldFilesHashes = \SUpdater::getReleaseFileHashes( \SUpdater::getReleaseVersion() );
        $newFilesHashes = \SUpdater::getReleaseFileHashes( $newVersion );

        $directives = \SUpdater::determineDirectives( $oldFilesHashes, $newFilesHashes );

        $differences = \SUpdater::findFilesDifferences( $directives['keep'] );
        if ($differences) {
            \SRequester::setSessionVariable( 'differences', $differences );
            \SRouter::redirect( array(
                'view' => 'update',
                'template' => 'solve'
            ) );
            //TODO: solve this
        }

        $releasePackHash = \SUpdater::getReleasePackHash( $newVersion );
        $releasePackPath = \SUpdater::downloadZipUpdatePack( $releasePackHash );

        //TODO: do backup, save all overwriting files to zip in tmp

        $success = \SUpdater::update( array_merge( $directives['add'], $directives['keep'] ), $releasePackPath, $directives );

        if ( $success ) {
            unlink( $releasePackPath );
        }

        \SRouter::redirect( array( 'view' => 'update' ) );
    }
}