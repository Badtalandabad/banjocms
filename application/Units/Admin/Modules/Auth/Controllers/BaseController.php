<?php

namespace Admin\Modules\Auth\Controllers;

use Admin\Library\AdminControllerAbstract;
use Admin\Library\AdminApplication;
use Core\Auth\User;

class BaseController extends AdminControllerAbstract
{
    /**
     * Login task
     */
    public function loginTask()
    {
        $email = \SRequester::getPostParam('email');
        $password = \SRequester::getPostParam('password');

        if (\SAuth::login($email, $password)) {
            $uri = \SAuth::getLastUri() ? \SAuth::getLastUri() : '/' . AdminApplication::ADMIN_PATH;
            \SRouter::redirect($uri);
        } else {
            \SReporter::addMessage('Email or password is incorrect', \SReporter::TYPE_ERROR);
            \SRouter::redirect(['module' => 'auth', 'view' => 'login'], AdminApplication::ADMIN_PATH);
        }
    }

    public function signUpTask()
    {
        if (\SAuth::getCurrentUser()->getRole() > User::ROLE_SUPERADMIN) {
            \SReporter::addMessage('Not enough rights', \SReporter::TYPE_ERROR);
            \SRouter::redirect(AdminApplication::ADMIN_PATH);
        }

        $email          = \SRequester::getPostParam('email');
        $password       = \SRequester::getPostParam('password');
        $passwordRepeat = \SRequester::getPostParam('password_repeat');
        $role           = (int)\SRequester::getPostParam('role');

        $redirect = false;
        if (!$email) {
            \SReporter::addMessage('Email is empty', \SReporter::TYPE_ERROR);
            $redirect = true;
        }
        if (!$password) {
            \SReporter::addMessage('Password is empty', \SReporter::TYPE_ERROR);
            $redirect = true;
        }
        if (!$role || $role >= User::LOWEST_RIGHTS_ROLE) {
            \SReporter::addMessage('Role has wrong value', \SReporter::TYPE_ERROR);
            $redirect = true;
        }
        if ($password !== $passwordRepeat) {
            \SReporter::addMessage('Passwords are not the same', \SReporter::TYPE_ERROR);
            $redirect = true;
        }
        if ($redirect) {
            \SRouter::redirect(['module' => 'auth', 'view' => 'registration'], AdminApplication::ADMIN_PATH);
        }

        if (\SAuth::register($email, $password, $role)){
            \SReporter::addMessage('User\'s created', \SReporter::TYPE_INFO);
        } else {
            \SReporter::addMessage('Sorry, an error occurred', \SReporter::TYPE_ERROR);
        }
        \SRouter::redirect(['module' => 'auth', 'view' => 'registration'], AdminApplication::ADMIN_PATH);
    }

    public function logoutTask()
    {
        \SAuth::logout();
        \SRouter::redirect(['module' => 'auth', 'view' => 'login'], AdminApplication::ADMIN_PATH);
    }
}