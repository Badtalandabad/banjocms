<?php

use Core\Auth\User;

$this->document->setTitle('Registration');
$script = 'if(getElementById(\'role\').options[sel.selectedIndex].value === ' . (int)User::ROLE_SUPERADMIN . '){confirm(\'New superadmin, are you sure?\');}';

?><form action="/admin/?task=signup&module=auth" name="registration_form" method="post" onsubmit="<?= $script; ?>">
    <input type="hidden" name="csrf_token" value="<?= '';?>">
    <input id="email" name="email" type="text" placeholder="Email"><br><br>
    <input id="password" name="password" type="password" placeholder="Password"><br><br>
    <input id="password-repeat" name="password_repeat" type="password" placeholder="Repeat password"><br><br>
    <select id="role" name="role">
        <option value="" selected>Choose role</option>
        <option value="<?= (int)User::ROLE_REGISTERED; ?>">Registered</option>
        <option value="<?= (int)User::ROLE_ADMIN; ?>">Admin</option>
        <option value="<?= (int)User::ROLE_SUPERADMIN; ?>">Superadmin</option>
    </select><br><br>
    <input type="submit" name="submit" value="Ok">
</form>



