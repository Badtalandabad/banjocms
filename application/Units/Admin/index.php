<?php
/**
 * Admin Index
 */

define('UNIT_NAME', 'Admin');
define('DEFAULT_MODULE', 'Admin');

define('TEMPLATE_PATH', $unitPath . '/' . TEMPLATE_DIR);

ob_start();

$app = new \Admin\Library\AdminApplication();
$app->go();
