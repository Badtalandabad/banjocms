<?php

namespace Constants;

interface DbTables
{
    const TRACKS  = 'tracks';
    const ARTISTS = 'artists';
    const MUSIC_SUPPLIERS = 'music_suppliers';
    const TRACKS_ARTISTS_BRIDGE = 'tracks_artists_bridge';
    const MUSIC_SUPPLIERS_TRACKS_BRIDGE = 'music_suppliers_tracks_bridge';
}