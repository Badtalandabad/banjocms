<?php

use Core\Auth\User;

/**
 * Class SAuth
 *
 * Authentication service
 */
class SAuth
{
    /**
     * Current user
     *
     * @var User
     */
    private static $user;

    /**
     * Returns auth data (kept in session)
     *
     * @return \Core\Auth\AuthData
     */
    private static function getAuthData()
    {
        return \Core\Auth\AuthData::getInstance();
    }

    /**
     * @param   int|string|null $idOrEmail  Id or email
     *
     * @return  User|null
     */
    private static function createUserObj($idOrEmail = null)
    {
        if (!(int)$idOrEmail) {
            return new User(null, User::ROLE_GUEST, null, null, null);
        }

        $userRepository = \Core\Auth\UserRepository::getInstance();

        if (ctype_digit((string)$idOrEmail)) {
            $user = $userRepository->getUserById($idOrEmail);
        } else {
            $user = $userRepository->getUserByEmail($idOrEmail);
        }

        return $user;
    }

    /**
     * Sets last visited URI
     *
     * @param   string  $uri    URI
     */
    public static function setLastUri($uri)
    {
        self::getAuthData()->setLastUri($uri);
    }

    /**
     * Returns last visited URI
     *
     * @return  string
     */
    public static function getLastUri()
    {
        return self::getAuthData()->getLastUri();
    }

    /**
     * Returns current user
     *
     * @return bool|User
     */
    public static function getCurrentUser()
    {
        if (!self::$user) {
            $sessionUserId = self::getAuthData()->getUserId();

            self::$user = self::createUserObj($sessionUserId);
        }

        return self::$user;
    }

    /**
     * Init auth stuff
     */
    public static function init()
    {
        self::getCurrentUser();
    }

    /**
     * Logs in user
     *
     * @param   string  $email      Email
     * @param   string  $password   Password
     *
     * @return  bool|User
     */
    public static function login($email, $password)
    {
        if (($user = self::createUserObj($email))
            && $user->verifyPassword($password)
        ) {
            self::getAuthData()->setUserId($user->getId());

            return $user;
        }

        return false;
    }

    /**
     * Register in user
     *
     * @param   string  $email      Email
     * @param   string  $password   Password
     * @param   int     $role       Role
     *
     * @return  bool|User
     */
    public static function register($email, $password, $role)
    {
        $user = self::createUserObj()
            ->setEmail($email)
            ->setPassword($password)
            ->setRole($role);

        $userRepository = \Core\Auth\UserRepository::getInstance();

        if ($userRepository->saveUser($user)) {
            return $user;
        }

        return false;
    }

    /**
     * Logs out user
     */
    public static function logout()
    {
        $data = self::getAuthData();

        $data::unsetInstance();
    }
}