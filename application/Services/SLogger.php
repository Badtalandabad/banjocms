<?php

/**
 * Class SLogger
 *
 * Logger service
 */
class SLogger
{
    const LOG_FILE_ERROR = 'error.log';

    public static function writeLog($message, $logFile = self::LOG_FILE_ERROR)
    {
        $message = date('H:i:s ') . $message . "\n";

        $logFilePath = LOG_DIR . '/' . $logFile;

        return file_put_contents($logFilePath, $message, FILE_APPEND);
    }

}