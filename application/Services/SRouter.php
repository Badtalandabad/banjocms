<?php

/**
 * Class SRouter
 * Contains methods for routing between
 * different application parts
 */
class SRouter
{
    /**
     * Returns link to the certain task
     *
     * @param   array|string    $paramsOrUri    Get parameters for the describing what to do
     * @param   string          $uriPrefix      Part of the URI before query (without start "/")
     * @param   string          $indexFile      Index file name
     *
     * @throws  \Core\Exception\NonFatalException    Wrong parameter
     *
     * @return  string
     */
    public static function route($paramsOrUri, $uriPrefix = null, $indexFile = '')
    {
        $path = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://'
            . $_SERVER['SERVER_NAME']
            . (empty($_SERVER['SERVER_PORT']) ? '' : ':' . $_SERVER['SERVER_PORT'])
            . (empty($uriPrefix) ? '' : '/' . $uriPrefix);

        if (is_array($paramsOrUri)) {
            $task = empty($paramsOrUri['task']) ? DEFAULT_TASK : $paramsOrUri['task'];

            $path .= ($indexFile ? '/' . $indexFile : '') . '?task=' . urldecode($task);
            foreach ($paramsOrUri as $name => $value) {
                if ($name == 'task') {
                    continue;
                }
                $path .= '&' . urldecode( $name ) . '=' . urldecode( $value );
            }
        } elseif (is_string($paramsOrUri)) {
            if (strpos($paramsOrUri, 'http://') !== 0 && strpos($paramsOrUri, 'https://') !== 0) {
                $path .= ($paramsOrUri[0] != '/' ? $_SERVER['REQUEST_URI'] : '')
                    . $paramsOrUri;
            } else {
                $path = $paramsOrUri;
            }
        } else {
            $eMessage = 'Redirect() got wrong param (not array not string)';
            throw new \Core\Exception\NonFatalException($eMessage);
        }

        return $path;
    }

    /**
     * Redirects somewhere
     *
     * @param   array|string    $paramsOrUri    Task parameters or URI where to redirect
     * @param   string          $uriPrefix      Part of the URI before query
     * @param   int             $httpCode       Redirect HTTP code
     *
     * @throws  \Core\Exception\NonFatalException    Wrong parameter
     */
    public static function redirect($paramsOrUri, $uriPrefix = null, $httpCode = 303)
    {
        $path = self::route($paramsOrUri, $uriPrefix);

        header('Location: ' . $path, true, $httpCode);

        exit(0);
    }

}