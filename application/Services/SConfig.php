<?php

/**
 * Class Configuration
 */
class SConfig
{
    const DEVELOPMENT_MODE = 'developmentMode';

    //const SHOW_ERROR      = 'showError';
    //const SHOW_WARNINGS   = 'showWarnings';
    const SHOW_PHP_ERRORS = 'showPhpErrors';

    const DB_DRIVER = 'dbDriver';
    const DB_HOST   = 'dbHost';
    const DB_NAME   = 'dbName';
    const DB_USER   = 'dbUser';
    const DB_PASSWORD = 'dbPassword';
    const DB_CHARSET  = 'dbCharset';
    const DB_PREFIX   = 'dbPrefix';

    /**
     * Configuration file name
     *
     * @var string
     */
    private static $_confFileName = 'config.ini';

    /**
     * Configuration file path. Relative to base directory.
     * It is relative, cause we cannot concatenate here and for preventing
     * showing full path on server to the user.
     *
     * @var string
     */
    private static $_confDir = 'config';

    /**
     * Configuration parameters
     * Parameter will remain type of its default value
     *
     * @var array
     */
    private static $_parameters = array(
        //Base
        self::DEVELOPMENT_MODE => true,
        //Errors
        self::SHOW_PHP_ERRORS => true,
        //Database
        self::DB_DRIVER   => '',
        self::DB_HOST     => 'localhost',
        self::DB_NAME     => '',
        self::DB_USER     => '',
        self::DB_PASSWORD => '',
        self::DB_CHARSET  => '',
        self::DB_PREFIX   => '',
    );

    /**
     * List of the possible types of parameter values.
     * Gettype function returns "double" in case of float.
     *
     * @var array
     */
    private static $_acceptableTypes = array(
        'boolean', 'integer', 'double', 'string',
    );

    /**
     * Initialization
     *
     * @throws \Core\Exception\SystemException If configuration file is missed
     */
    public static function init()
    {
        $confPath = BASE_PATH . '/' . self::$_confDir . '/' . self::$_confFileName;

        if (!file_exists($confPath)) {
            $exceptionMessage = 'Configuration file "' . self::$_confFileName . '" is missed.'
                . 'It must be in the ' . self::$_confDir . ' directory.';
            throw new \Core\Exception\SystemException($exceptionMessage);
        }

        $arrayFromIni = parse_ini_file($confPath);
        self::_setParameters($arrayFromIni);
    }

    /**
     * Checks fitness of values in the configuration file and sets those in _parameters
     *
     * @param array $arrayFromIni List if the configuration file values
     *
     * @throws \Core\Exception\SystemException If type of the default value not allowed
     */
    private static function _setParameters($arrayFromIni)
    {
        foreach ($arrayFromIni as $name => $value) {
            if (array_key_exists($name, self::$_parameters)) {
                //Checks type, for avoiding mistakes with complex types(object, resource) on default values
                $type = gettype(self::$_parameters[$name]);
                if (!in_array($type, self::$_acceptableTypes)) {
                    $exceptionMessage = 'Unacceptable configuration parameter type: "' . $type . '".';
                    throw new \Core\Exception\SystemException($exceptionMessage);
                }
                settype($value, $type);
                self::$_parameters[$name] = $value;
            }
        }
    }

    /**
     * Returns parameter from configuration array
     *
     * @param   string  $name       Parameter name
     * @param   mixed   $default    Default parameter value
     *
     * @return  mixed
     */
    public static function getParameter($name, $default = null)
    {
        if (array_key_exists($name, self::$_parameters)) {
            return self::$_parameters[$name];
        } else {
            return $default;
        }
    }

}