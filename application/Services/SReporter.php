<?php

/**
 * Class SErrorReporter
 *
 * Gathers error reporting messages
 */
class SReporter
{
    const TYPE_ERROR = 1;
    const TYPE_WARNING = 2;
    const TYPE_INFO = 3;

    /**
     * Messages.
     *
     * @var array
     */
    private static $messages = [];

    private static $sessionName = UNIT_NAME . '_messages';

    public static function init()
    {
        self::$messages = \SRequester::getSessionVariable(self::$sessionName, []);
    }

    /**
     * Adds warning message to its storage
     *
     * @param string  $message Message text
     * @param int     $type    Message type
     */
    public static function addMessage($message, $type = self::TYPE_INFO)
    {
        if (!isset(self::$messages[$type])) {
            self::$messages[$type] = [];
        }

        self::$messages[$type][] = $message;

        \SRequester::setSessionVariable(self::$sessionName, self::$messages);
    }

    /**
     * Returns messages
     *
     * @param int $type Messages type
     *
     * @return array
     */
    public static function getMessages($type = null)
    {
        $messages = [];
        if (!$type) {
            foreach (self::$messages as $messagesByType) {
                $messages = array_merge($messages, $messagesByType);
            }
        } elseif (isset(self::$messages[$type])) {
            $messages = self::$messages[$type];
        }

        return $messages;
    }

    public static function removeMessages()
    {
        self::$messages = [];
        \SRequester::setSessionVariable(self::$sessionName, self::$messages);
    }
}