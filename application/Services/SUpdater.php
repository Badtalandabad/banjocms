<?php

use \Core\JsonResponse;
use \Core\Updater\UpdaterException;

/**
 * Class SUpdater
 */
class SUpdater
{
    /**
     * Release version of the system
     *
     * @var string
     */
    private static $_releaseVersion = '0.0.5';

    /**
     * Link to update server. You can give it task
     * to execute by get parameters.
     *
     * @var string
     */
    private static $_serverLink = 'http://banjo-update.local/update.php';

    /**
     * Gets file hashes of the certain release
     *
     * @param   string  $version    System version, which hashes we want to get
     *
     * @throws  \Core\Updater\RequestFailException  If server returned
     *                                              error flag or nothing.
     *
     * @return  array
     */
    public static function getReleaseFileHashes($version)
    {
        $hashes= self::execCommandOnServer(
            'get-release-hashes',
            ['version' => $version]
        );

        return $hashes;
    }

    /**
     * Gets hash of the certain release pack file
     *
     * @param   string  $version    System version, which release file hash we want to get
     *
     * @throws  \Core\Updater\RequestFailException  If server returned
     *                                              error flag or nothing.
     *
     * @return  string
     */
    public static function getReleasePackHash($version)
    {
        $hash = self::execCommandOnServer(
            'get-release-pack-hash',
            ['version' => $version]
        );

        return $hash;
    }

    /**
     * Returns array of modified files which are going to be updated
     *
     * @param   array   $hashesOfUpdatingFiles  List of hashes for files to update
     *                                          array('old'=>md5, 'new'=>md5)
     *
     * @return  array
     */
    public static function findFilesDifferences($hashesOfUpdatingFiles)
    {
        $mismatches = array();
        foreach ($hashesOfUpdatingFiles as $path => $hash) {
            $fullFilePath = BASE_PATH . '/' . $path;

            if (!is_file($fullFilePath)) {
                continue;
            }

            $oldHash = md5(file_get_contents($fullFilePath));
            if ($oldHash != $hash) {
                $mismatches[] = $path;
            }
        }

        return $mismatches;
    }

    /**
     * Downloads zip update pack from update server and
     * saves it in tmp directory. Returns path to tmp file.
     *
     * @param   string  $zipHash    Hash of downloading zip file
     *
     * @throws  UpdaterException    If response is empty
     *
     * @return  string
     */
    public static function downloadZipUpdatePack( $zipHash )
    {
        $commandLink = self::makeServerCommandLink( 'run-update' );

        // Load release pack file from server
        $response = file_get_contents( $commandLink );

        if ( ! $response ) {
            throw new UpdaterException( 'Getting zip update file failed.' );
        }

        if ( md5( $response ) != $zipHash ) {
            $exceptionMessage = 'Hash of downloaded zip update pack'
                . ' is not the same as expected.';
            throw new UpdaterException( $exceptionMessage );
        }

        //TODO: check integrity, compare hashes

        $filePath = self::getReleaseFilePath( '0.0.3' );
        $tmpSuccess = file_put_contents( $filePath, $response );
        if ( ! $tmpSuccess ) {
            throw new UpdaterException( 'Temp zip was not saved!' );
        }

        //TODO: unset file string variable

        return $filePath;
    }

    /**
     * Sets version of the system
     *
     * @param   string  $version    Release version of the system
     */
    public static function setReleaseVersion( $version )
    {
        self::$_releaseVersion = $version;
    }

    /**
     * Returns release version of the system
     *
     * @return  string
     */
    public static function getReleaseVersion()
    {
        return self::$_releaseVersion;
    }

    /**
     * Returns update server link
     *
     * @return  string
     */
    public static function getServerLink()
    {
        return self::$_serverLink;
    }

    /**
     * Sets update server link
     *
     * @param   string  $serverLink Link to update server
     */
    public static function setServerLink( $serverLink )
    {
        self::$_serverLink = $serverLink;
    }

    /**
     * Returns name of release pack file. Just for unifying file naming.
     *
     * @param   string  $version    Release version
     *
     * @return  string
     */
    public static function getReleaseFileName( $version = null )
    {
        $version = $version ? $version : self::getReleaseVersion();

        $fileName = 'release-pack-v.' . $version . '.zip';

        return $fileName;
    }

    /**
     * Returns full path of release pack file.
     *
     * @param   string  $version    Release version
     *
     * @return  string
     */
    public static function getReleaseFilePath($version = null)
    {
        $filePath = TEMP_DIR . '/' . self::getReleaseFileName($version);

        return $filePath;
    }

    /**
     * Creates link to update server for performing given command.
     *
     * @param   string  $command    Command to execute
     * @param   array   $params     Additional URL GET parameters
     *
     * @return  string
     */
    public static function makeServerCommandLink($command, $params = [])
    {
        $link = self::getServerLink() . '?task=' . urlencode($command);

        foreach ($params as $name => $value) {
            $link .= '&' . urlencode($name) . '=' . urlencode($value);
        }

        return $link;
    }

    /**
     * Creates link to update server and makes request to it.
     *
     * @param   string  $command    Command to execute
     * @param   array   $params     Additional URL GET parameters
     *
     * @throws  \Core\Updater\RequestFailException  If server returned
     *                                              error flag or nothing.
     *
     * @return  mixed
     */
    public static function execCommandOnServer($command, $params = [])
    {
        $link = self::makeServerCommandLink($command, $params);

        $responseString = file_get_contents($link);

        //if ( ! $responseString ) {
        //    $exception = new RequestFailException( 'Update server returned an empty answer.' );
        //    $exception->setRequestName( $command );
        //    throw $exception;
        //}

        if (!$responseString) {
            return false;
        }

        $response = new JsonResponse($responseString);

        //if ( $response->isError() ) {
        //    $exception = new RequestFailException( $response->getErrorMessage() );
        //    $exception->setRequestName( $command );
        //    throw $exception;
        //}

        return $response->getData();
    }

    /**
     *
     *
     * @param   array   $oldFilesHashes Old version system file hashes
     * @param   array   $newFilesHashes New version system file hashes
     *
     * @return  array
     */
    public static function determineDirectives( $oldFilesHashes, $newFilesHashes )
    {
        $directives = [
            'add' => [],
            'keep' => [],
            'remove' => [],
        ];
        foreach ($newFilesHashes as $filePath => $hash) {
            if (!array_key_exists( $filePath, $oldFilesHashes)) { //it's new file, add it
                $directives['add'][$filePath] = $hash;
            }
        }

        foreach ($oldFilesHashes as $filePath => $hash) {
            if (!array_key_exists($filePath, $newFilesHashes)) {
                //file is not in new release, remove it
                $directives['remove'][$filePath] = $hash;
            } else {
                // file is kept, it will be updated or will remain the same
                $directives['keep'][$filePath] = $hash;
            }
        }

        return $directives;
    }

    /**
     * Checks if any system files were changed
     *
     * @throws  \Core\Updater\RequestFailException  If server returned
     *                                              error flag or nothing.
     *
     * @return  array
     */
    public static function checkSystemIntegrity()
    {
        $hashes = self::getReleaseFileHashes(self::getReleaseVersion());

        $mismatches = self::findFilesDifferences($hashes);

        return $mismatches;
    }

    /**
     * Checks if update is needed.
     *
     * @throws  \Core\Updater\RequestFailException  If server returned
     *                                              error flag or nothing.
     *
     * @return  bool
     */
    public static function checkForUpdate()
    {
        $isUpdateNeeded = self::execCommandOnServer(
            'check-for-update',
            ['version' => self::getReleaseVersion()]
        );

        return $isUpdateNeeded;
    }

    /**
     * Runs update process. Downloads release pack. Saves it.
     * Extracts it, with file overwriting.
     *
     * @param   array   $newFilesHashes New version system file hashes
     * @param   string  $zipPath        Release pack file path
     * @param   array   $directives     Directives what to do. Using to determine what to remove
     *
     * @throws  \Core\Updater\UpdaterException  If download failed
     * @throws  \Core\Updater\UpdaterException  If release pack file checksum was not found in the response
     * @throws  \Core\Updater\UpdaterException  If release pack checksum does not match with given checksum
     * @throws  \Core\Updater\UpdaterException  If release pack saving failed
     * @throws  \Core\Updater\UpdaterException  If release pack opening failed
     *
     * @return  bool
     */
    public static function update($newFilesHashes, $zipPath, $directives)
    {
        $zip = new ZipArchive();
        $openResult = $zip->open($zipPath, ZipArchive::CHECKCONS);
        if ( $openResult !== true ) {
            $zip->close();
            $exceptionMessage = 'Zip open error! Error code: ' . $openResult;
            throw new UpdaterException( $exceptionMessage );
        }

        foreach ($newFilesHashes as $path => $fileHash) {
            $newFileContent = $zip->getFromName($path);

            if (is_file(BASE_PATH . '/' . $path)
                && md5($newFileContent) == md5(file_get_contents(BASE_PATH . '/' . $path))
            ) {
                continue;
            }
            file_put_contents(BASE_PATH . '/' . $path, $newFileContent);
            //TODO: check if ok, if not try to do revert
        }

        $zip->close();

        foreach ($directives['remove'] as $filePathToRemove => $hash) {
            $removed = unlink(BASE_PATH . '/' . $filePathToRemove);
        }

        return true;
    }

}