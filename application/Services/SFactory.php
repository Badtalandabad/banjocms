<?php

/**
 * Class Factory
 * Static class
 * Provides access to the different application elements
 */
class SFactory
{
    private static $repositories = [];
    
    /**
     * Creates and returns view with specified name
     * for the current (or specified) module
     *
     * @param string $name       View name
     * @param string $moduleName Name of the module
     *
     * @return null|\Core\View\ViewAbstract
     *
     * @throws \Core\Exception\NonFatalException   If cannot find view
     */
    public static function getModuleView($name, $moduleName = null)
    {
        $name = ucfirst($name);
        if (!$moduleName) {
            $moduleName = \SRegistry::getModuleName();
        }
        if (!$moduleName) {
            $moduleName = DEFAULT_MODULE;
        }

        $className = UNIT_NAME . '\\Modules\\' . $moduleName . '\\Views\\' . $name . '\\' . $name . 'View';

        $path = \SLoader::findClassFilePath($className);

        if ($path) {
            return new $className();
        } else {
            $message = 'Failed on trying to get view: "' . $name . '"';
            //Suggestion: to add specified exception class
            throw new \Core\Exception\NonFatalException($message);
        }
    }

    /**
     * Returns database object
     *
     * @return \Core\Db\Db
     */
    public static function getDb()
    {
        $db = \Core\Db\Db::getInstance();

        return $db;
    }

    /**
     * Checks if repository exists in register
     *
     * @param string $fullClassName Full class name
     *
     * @return bool
     */
    public static function checkRepositoryExists($fullClassName)
    {
        return !empty(self::$repositories[$fullClassName]);
    }

    /**
     * Returns repository from the registry. Instantiates it if needed
     *
     * @param string $fullClassName    Full class name (with namespaces)
     * @param bool   $createIfNotExist Create if repository doesn't exists
     *
     * @return \Core\Repository\RepositoryAbstract|null
     */
    public static function getRepository($fullClassName, $createIfNotExist = true)
    {
        $repository = null;

        if (!empty(self::$repositories[$fullClassName])) {
            $repository = self::$repositories[$fullClassName];
        } elseif (
            class_exists($fullClassName)
            && method_exists($fullClassName, 'getInstance')
            && $createIfNotExist
        ) {
            $repository = $fullClassName::getInstance();
            self::$repositories[$fullClassName] = $repository;
        }

        return $repository;
    }

    /**
     * Saves repository in register
     *
     * @param \Core\Repository\RepositoryAbstract $repository Repository
     */
    public static function saveRepository(\Core\Repository\RepositoryAbstract $repository)
    {
        self::$repositories[get_class($repository)] = $repository;
    }
}