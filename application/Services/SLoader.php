<?php


/**
 * Class Loader
 * Finds and loads declaration files for new classes.
 * All relative paths are relative to 'Application' directory
 */
class SLoader
{
    /**
     * Directories for searching, common for all classes
     *
     * @var array
     */
    private static $commonPaths = array(
        APP_PATH_RELATIVE => APP_PATH_RELATIVE,
        UNITS_PATH_RELATIVE => UNITS_PATH_RELATIVE,
    );

    /**
     * Paths specified for the current class
     *
     * @var array
     */
    private static $classSpecifiedPaths = array();

    /**
     * List of loaded files
     *
     * @var array
     */
    private static $loadedFiles = array();

    /**
     * Returns common paths
     *
     * @return  array
     */
    private static function getCommonPaths()
    {
        return self::$commonPaths;
    }

    /**
     * Adds specified class path to the store
     *
     * @param   string  $path   Full path to the file
     */
    private static function addClassSpecifiedPath($path)
    {
        self::$classSpecifiedPaths[$path] = $path;
    }

    /**
     * Returns specified class paths
     *
     * @return array
     */
    private static function getClassSpecifiedPaths()
    {
        return self::$classSpecifiedPaths;
    }

    /**
     * Clears class specified paths
     */
    private static function clearClassSpecifiedPaths()
    {
        self::$classSpecifiedPaths = array();
    }

    /**
     * Saves loaded file path in store
     *
     * @param   string  $path   Full path to the file
     */
    private static function registerLoadedFile($path)
    {
        self::$loadedFiles[$path] = $path;
    }

    /**
     * Checks if file have already loaded. Returns true on success, otherwise false
     *
     * @param   string  $path   Full path to the file
     *
     * @return  bool
     */
    private static function checkLoadedFile($path)
    {
        if (array_key_exists($path, self::$loadedFiles)) {
            return true;
        }
        return false;
    }

    /**
     * Adds common path to the path store. Nothing would happen if
     * file have already stored
     *
     * @param   string $path Relative(to the 'Application' directory)
     *                       path for searching
     */
    public static function addCommonPath($path)
    {
        self::$commonPaths[$path] = $path;
    }

    /**
     * Adds a bunch of common paths to the path store.
     * Nothing would happen if some of paths have already stored
     *
     * @param   array $paths Array of the relative(to the
     *                       'Application' directory) paths for searching
     *
     * @deprecated
     */
    public static function addCommonPaths($paths)
    {
        foreach ($paths as $path) {
            self::addCommonPath($path);
        }
    }

    /**
     * Returns full paths for searching
     *
     * @param   string  $className  Full (with the namespaces) name of the class
     *
     * @return  array
     */
    public static function getSearchPaths($className)
    {
        $namespaces = explode('\\', $className);

        // namespace in one word - service
        if (count($namespaces) == 1) {
            self::addClassSpecifiedPath(SERVICES_PATH_RELATIVE);
        }

        $relativePath = implode('/', $namespaces) . '.php';
        $cleanClassFileName = end($namespaces) . '.php';
        reset($namespaces);

        $fullPaths = [];
        foreach (self::getClassSpecifiedPaths() as $searchDir) {
            $searchDir = $searchDir ? '/' . $searchDir : '';
            $fullPaths[] = BASE_PATH . $searchDir . '/' . $cleanClassFileName;
            if ($relativePath != $cleanClassFileName) {
                $fullPaths[] = BASE_PATH . $searchDir . '/' . $relativePath;
            }
        }
        foreach (self::getCommonPaths() as $searchDir) {
            $fullPaths[] = BASE_PATH . ($searchDir ? '/' . $searchDir : '') . '/' . $relativePath;
        }

        return $fullPaths;
    }


    /**
     * Returns path to the class declaration file
     *
     * @param   string  $className  Full (with the namespaces) name of the class
     *
     * @return  string|null
     */
    public static function findClassFilePath($className)
    {
        foreach (self::getSearchPaths($className) as $path) {
            if (is_file($path)) {
                return $path;
            }
        }

        return null;
    }

    /**
     * Loads file with class declaration. Returns true on success, otherwise false
     *
     * @param   string  $className  Full (with the namespaces) name of the class
     *
     * @throws  \Exception   If trying to load some file twice
     *
     * @return  bool
     */
    public static function load($className)
    {
        $paths = self::getSearchPaths($className);
        foreach ($paths as $path) {
            if (is_file($path)) {
                /*if (self::checkLoadedFile($path)) {
                    $message = 'Trying to load file that already loaded. Class name: '. $className;
                    $exception = new \Exception($message);
                    //$exception->setClassName( $className )
                    //    ->setSearchPaths( $paths );
                    throw $exception;
                }*/

                require $path;

                self::registerLoadedFile( $path );
                self::clearClassSpecifiedPaths();

                return true;
            }
        }

        return false;
    }

}