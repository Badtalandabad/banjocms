<?php

/**
 * Class SRequester
 * Contains request management methods
 */
class SRequester
{
    public static function removeNameHyphens($nameWithHyphens)
    {
        $name = '';
        foreach (explode('-', $nameWithHyphens) as $word) {
            $name .= ucfirst(strtolower($word));
        }

        return $name;
    }

    /**
     * Returns request URI
     *
     * @return string
     */
    public static function getUri()
    {
        return empty($_SERVER['REQUEST_URI']) ? '' : $_SERVER['REQUEST_URI'];
    }

    /**
     * Returns request parameter
     *
     * @param string $fieldName     Name of the request parameter
     * @param string $defaultValue  Value in case the field wasn't set
     * @param bool   $trim          Do trim or not
     * @param string $requestMethod POST/GET
     *
     * @return  string|null
     */
    public static function getParam($fieldName, $defaultValue = null, $trim = true, $requestMethod = null)
    {
        switch (strtolower((string)$requestMethod)) {
            case 'get':
                $requestArr = $_GET;
                break;
            case 'post':
                $requestArr = $_POST;
                break;
            default:
                $requestArr = $_REQUEST;
        }

        if (empty( $requestArr[$fieldName])) {
            $request = $defaultValue;
        } else {
            $request = $requestArr[$fieldName];
            if ($trim) {
                $request = trim($request);
            }
        }

        return $request;
    }

    /**
     * Returns GET request parameter
     *
     * @param string $fieldName    Name of the request parameter
     * @param string $defaultValue Value in case the field wasn't set
     * @param bool   $trim         Do trim or not
     *
     * @return  string|null
     */
    public static function getGetParam($fieldName, $defaultValue = null, $trim = true)
    {
        return self::getParam($fieldName, $defaultValue, $trim, 'get');
    }

    /**
     * Returns POST request parameter
     *
     * @param string $fieldName    Name of the request parameter
     * @param string $defaultValue Value in case the field wasn't set
     * @param bool   $trim         Do trim or not
     *
     * @return  string|null
     */
    public static function getPostParam($fieldName, $defaultValue = null, $trim = true)
    {
        return self::getParam($fieldName, $defaultValue, $trim, 'post');
    }

    /**
     * Sets element in the request array
     *
     * @param   string  $name   Name of the element
     * @param   string  $value  Value of the element
     */
    public static function setVariable($name, $value)
    {
        $_REQUEST[$name] = $value;
    }

    /**
     * Sets session variable
     *
     * @param   string  $name   Variable name in $_SESSION array
     * @param   mixed   $value  Variable value
     */
    public static function setSessionVariable($name, $value)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION[$name] = $value;
    }

    /**
     * Returns session variable
     *
     * @param   string  $name       Variable name in $_SESSION array
     * @param   mixed   $default    Default value
     *
     * @return  mixed
     */
    public static function getSessionVariable($name, $default = null)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (array_key_exists($name, $_SESSION)) {
            return $_SESSION[$name];
        }

        return $default;
    }

    /**
     * Deletes variable from $_SESSION array
     *
     * @param   string  $name   Variable name in $_SESSION array
     */
    public static function deleteSessionVariable($name)
    {
        unset($_SESSION[$name]);
    }

}