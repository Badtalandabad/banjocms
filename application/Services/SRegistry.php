<?php

class SRegistry
{
    /**
     * Returns unit path
     *
     * @param string $unitName Unit name
     *
     * @return string
     */
    public static function getUnitPath($unitName)
    {
        return UNITS_PATH . '/' . $unitName;
    }
    
    /**
     * Returns name of the current module
     *
     * @return string
     */
    public static function getModuleName()
    {
        return ucfirst(strtolower(\SRequester::getGetParam('module', DEFAULT_MODULE)));
    }

    /**
     * Compares given name to current module name (regardless case)
     *
     * @param string $nameToTest Name to compare
     *
     * @return bool
     */
    public static function isCurrentModule($nameToTest)
    {
        return strtolower(self::getModuleName()) === strtolower($nameToTest);
    }

    /**
     * Returns current controller
     *
     * @return string
     */
    public static function getControllerName()
    {
        $controller = \SRequester::getGetParam('controller', DEFAULT_CONTROLLER);

        return \SRequester::removeNameHyphens($controller);
    }

    /**
     * Returns current task
     *
     * @return string
     */
    public static function getTask()
    {
        $task = \SRequester::getGetParam('task', DEFAULT_TASK);

        return \SRequester::removeNameHyphens($task);
    }

    /**
     * Compares given name to current task name (regardless case)
     *
     * @param string $nameToTest Name to compare
     *
     * @return bool
     */
    public static function isCurrentTask($nameToTest)
    {
        return strtolower(self::getTask()) === strtolower($nameToTest);
    }

}