<?php

namespace Core;


/**
 * Trait ModuleElementName
 * Provides methods for every module element(views, controllers)
 * By this way everywhere in the module we can get name of the module
 * or name of the executing element.
 */
trait ModuleElementNameTrait
{
    /**
     * Returns name of the module in which this method was called.
     *
     * @return  string|null
     */
    public function getModuleName()
    {
        $className = get_class($this);
        $namespace = explode('\\', $className);

        $key = array_search(MODULES_DIR, $namespace, true);
        if (!empty($namespace[$key + 1])) {
            return $namespace[$key + 1];
        }

        return null;
    }

    /**
     * Returns name of the element of the module from which
     * this method was called.
     *
     * @return  string|null
     */
    public function getName()
    {
        $namespace = explode('\\', get_class($this));
        $className = array_pop($namespace);

        $elementTypes = array('Controller', 'View');
        foreach($elementTypes as $type) {
            // check type is in the end
            $nameLength = strlen($className) - strlen($type);
            $typePosition = strrpos($className, $type);
            if ($typePosition === $nameLength) {
                return substr_replace($className, '', $typePosition);
            }
        }

        return null;
    }
} 