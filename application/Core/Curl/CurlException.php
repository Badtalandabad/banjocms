<?php

namespace Core\Curl;

use Core\Exception\NonFatalException;

/**
 * CURL exception
 *
 * @package Exception\Parser
 */
class CurlException extends NonFatalException
{
    /**
     * Initialisation
     */
    public function init()
    {
        $this->message = 'CURL exception: ' . $this->message;
    }
}
