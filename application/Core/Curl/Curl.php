<?php

namespace Core\Curl;

/**
 * cURL control object
 *
 * @package Parser
 */
class Curl
{
    /**
     * Limit of the curl connections;
     */
    const CURL_CONNECTION_LIMIT = 1000;

    /**
     * Counter of curl connections
     *
     * @var int
     */
    private $_curlConnectionCounter = 0;

    /**
     * Last connected host.
     *
     * @var string
     */
    private $_lastHost = '';

    /**
     * Current curl session.
     *
     * @var resource
     */
    private $_curlSession;

    /**
     * Array with cURL options, which should be set by curl_setopt_array ONLY FOR ONE connection.
     * Array takes view like OPTION_CONSTANT => value.
     * Setting URL and RETURNTRANSFER options is restricted
     *
     * @var array
     */
    private $_optionsOnce = array();

    /**
     * Array with headers, which should be set by curl_setopt_array FOR ALL connections
     * Array takes view like OPTION_CONSTANT => value.
     * Setting URL and RETURNTRANSFER options is restricted
     *
     * @var array
     */
    private $_optionsPermanent = array();

    /**
     * Adds option for once setting, to optionsOnce array
     *
     * @param int   $optionConstant cURL option constant
     * @param mixed $optionValue    Value of the option
     *
     * return void
     */
    public function setOptionOnce($optionConstant, $optionValue)
    {
        $this->_optionsOnce[$optionConstant] = $optionValue;
    }

    /**
     * Adds option for setting for permanent, to optionsPermanent array
     *
     * @param int   $optionConstant cURL option constant
     * @param mixed $optionValue    Value of the option
     *
     * return void
     */
    public function setOptionPermanent($optionConstant, $optionValue)
    {
        $this->_optionsPermanent[$optionConstant] = $optionValue;
    }

    /**
     * Unsets option from optionsPermanent array
     *
     * @param int   $optionConstant cURL option constant
     *
     * return void
     */
    public function unsetOptionPermanent( $optionConstant )
    {
        unset( $this->_optionsPermanent[$optionConstant] );
    }

    /**
     * Constructor
     *
     * @return  Curl
     */
    public function __construct()
    {

    }

    /**
     * Returns current host
     *
     * @return  string
     */
    public function getLastHost()
    {
        return $this->_lastHost;
    }

    /**
     * Connect to host by curl
     *
     * @param   $host   string   link to connect, to current parsing page
     *
     * @throws  CurlException  If count of connections over than limit
     */
    public function connectToHost( $host )
    {
        $this->_curlConnectionCounter++;
        if ($this->_curlConnectionCounter > self::CURL_CONNECTION_LIMIT) {
            $exceptionMessage = 'Connection limit is reached(' . self::CURL_CONNECTION_LIMIT . ').';
            throw new CurlException($exceptionMessage);
        }

        $this->_lastHost = $host;

        $this->_curlSession = curl_init();

        curl_setopt_array($this->_curlSession, $this->_optionsPermanent);

        curl_setopt_array($this->_curlSession, $this->_optionsOnce);
        $this->_optionsOnce = array();

        curl_setopt($this->_curlSession, CURLOPT_URL, $host);
        curl_setopt($this->_curlSession, CURLOPT_RETURNTRANSFER, TRUE);
    }

    /**
     * Execute the given cURL session
     *
     * @return  string
     */
    public function execute()
    {
        $pageAsString = curl_exec($this->_curlSession);

        return $pageAsString;
    }

    /**
     * Disconnect from host
     */
    public function disconnect()
    {
        curl_close($this->_curlSession);
    }

}
