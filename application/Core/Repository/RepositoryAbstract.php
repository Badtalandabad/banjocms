<?php

namespace Core\Repository;

/**
 * Class RepositoryAbstract
 */
abstract class RepositoryAbstract
{
    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    /**
     * Returns repository instance
     * Saves it in the factory
     *
     * @return  static
     */
    static public function getInstance()
    {
        if (!\SFactory::checkRepositoryExists(get_called_class())) {
            $repository = new static();
            \SFactory::saveRepository($repository);
        } else {
            $repository = \SFactory::getRepository(get_called_class(), false);
        }

        return $repository;
    }
}