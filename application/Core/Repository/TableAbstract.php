<?php

namespace Core\Repository;

abstract class TableAbstract
{
    /**
     * Table name
     *
     * @var string
     */
    protected $tableName;

    /**
     * Returns name of the table
     *
     * @return  string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * Sets name of the table
     *
     * @param   string  $tableName  Name of the table
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * Loads entity
     *
     * @param int $id Track id
     *
     * @return \stdClass
     */
    public function find($id)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $query->select()
            ->from($this->getTableName())
            ->where('id = ' . (int)$id);

        if (!$result = $db->execute($query)) {
            return null;
        }

        return $result->fetchRowObj();
    }

    public function delete($id)
    {
        $db = \SFactory::getDb();
        $query = $db->getQuery();

        $query->delete()
            ->from($this->getTableName())
            ->where('id = ' . (int)$id);

        return (bool)$db->execute($query);
    }

}