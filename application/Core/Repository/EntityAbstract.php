<?php

namespace Core\Repository;

use Core\Exception\NonFatalException;

abstract class EntityAbstract
{
    /**
     * Entity's id
     *
     * @var int
     */
    protected $id;

    /**
     * When row was added to the database
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * When row was updated last time
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * Entity constructor.
     *
     * @param int       $id        Entity id
     * @param \DateTime $createdAt Creation time
     * @param \DateTime $updatedAt Last update time
     *
     * @throws NonFatalException If arguments are not valid
     */
    public function __construct($id, $createdAt, $updatedAt)
    {
        if ($id) {
            $this->id = (int)$id;
        }

        //TODO: remove copy-paste
        if ($createdAt instanceof \DateTime) {
            $this->createdAt = $createdAt;
        } elseif (is_string($createdAt)) {
            try {
                $createdAt = new \DateTime($createdAt);
                $this->createdAt = $createdAt;
            } catch (\Exception $e) {
                throw new NonFatalException('Entity creation time is not valid.');
            }
        }

        if ($updatedAt instanceof \DateTime) {
            $this->updatedAt = $updatedAt;
        } elseif (is_string($updatedAt)) {
            try {
                $updatedAt = new \DateTime($updatedAt);
                $this->updatedAt = $updatedAt;
            } catch (\Exception $e) {
                throw new NonFatalException('Entity update time is not valid.');
            }
        }
    }

    /**
     * Returns entity's id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets entity's id
     *
     * @param int $id Entity's id
     *
     * @return self
     *
     * @throws NonFatalException If argument id is not valid
     */
    public function setId($id): self
    {
        $id = (int)$id;
        if (!$id) {
            throw new NonFatalException('Entity id is not a valid number');
        }

        $this->id = $id;

        return $this;
    }

    /**
     * Returns creation time
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets added on time
     *
     * @param \DateTime $createdAt
     *
     * @return self
     */
    protected function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Returns update time
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Sets update time
     *
     * @param \DateTime $updatedAt
     */
    protected function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

}