<?php

namespace Core\View;

/**
 * Class Document. Describes output page
 *
 * @package View
 */
class Document
{
    /**
     * Title of the page
     *
     * @var string
     */
    private $title;

    /**
     * Content of the body tag
     *
     * @var string
     */
    private $content;

    /**
     * Tags in the head tag
     *
     * @var array
     */
    private $headTags = [];

    /**
     * Custom data to put outside of content box
     *
     * @var array
     */
    private $data = [];

    /**
     * View patches
     *
     * @var array
     */
    private $renderedViewPatches = [];

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addHeadTag('meta', false, ['charset' => 'utf-8']);
    }

    /**
     * Sets title of the page
     *
     * @param   string  $title  Title of the page
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = htmlspecialchars($title);

        return $this;
    }

    /**
     * Returns title of the page
     *
     * @return  mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets content of the page (content of the body tag)
     *
     * @param   string  $content    Content of the page
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Returns content of the page (content of the body tag)
     *
     * @return  string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Constructs and adds head tag in the headTags array
     *
     * @param   string  $tagName                    Tag name
     * @param   string  $tagValue                   Tag value
     * @param   array   $params                     Tag parameters
     * @param   bool    $addClosingTagOnEmptyValue  If true adds closing tag on empty values
     *
     * @throws  \Core\Exception\SystemException  If parameters variable is not an array
     *
     * @return $this
     */
    public function addHeadTag($tagName, $tagValue, $params = [], $addClosingTagOnEmptyValue = false)
    {
        if ( ! is_array( $params ) ) {
            throw new \Core\Exception\SystemException('Parameters variable must be an array.');
        }

        $tagParams = '';

        foreach ($params as $paramName => $paramValue) {
            $paramValue = str_replace('"', '\'', $paramValue);
            $tagParams .= ' ' . $paramName . '="' . $paramValue . '"';
        }

        $tag = '<' . $tagName . $tagParams;

        if (!$addClosingTagOnEmptyValue
            && ($tagValue === false || $tagValue === '' || $tagValue === null)
        ) {
            $tag .= ' />';
        } else {
            $tag .= ' >' . $tagValue . '</' . $tagName . '>';
        }
        $this->headTags[] = $tag;

        return $this;
    }

    /**
     * Glues head tags and returns them
     *
     * @return  string
     */
    public function getHeadTags()
    {
        return implode("\n", $this->headTags) . "\n";
    }

    /**
     * Returns variable from data store if it exists, otherwise null
     *
     * @param string $key Name of the variable in data store
     *
     * @return mixed
     */
    public function getVarFromDataStore($key)
    {
        return $this->data[$key] ?? null;
    }

    /**
     * Sets variable in data store
     *
     * @param string $key Name of the variable in data store
     * @param string $var Variable to set in data store
     *
     * @return $this
     */
    public function setVarToDataStore($key, $var)
    {
        $this->data[$key] = $var;

        return $this;
    }

    /**
     * Returns array os rendered view patches
     *
     * @return array
     */
    public function getRenderedViewPatches()
    {
        return $this->renderedViewPatches;
    }

    /**
     * Sets array of rendered view patches
     *
     * @param array $renderedViewPatches
     *
     * @return $this
     */
    public function setRenderedViewPatches($renderedViewPatches)
    {
        $this->renderedViewPatches = $renderedViewPatches;

        return $this;
    }

    /**
     * Returns view patch render
     *
     * @param string $viewPatchName View patch name
     *
     * @return string|null
     */
    public function getViewPatchRender($viewPatchName)
    {
        return $this->renderedViewPatches[$viewPatchName] ?? null;
    }

    /**
     * Adds 'link' tag that includes css file to the head
     *
     * @param string $publicDirRelativePath Relative to the 'public' directory path to css file
     * 
     * @throws \Core\Exception\SystemException
     *
     * @return $this
     */
    public function addCssFile($publicDirRelativePath)
    {
        $this->addHeadTag(
            'link',
            '',
            array(
                'rel' => 'stylesheet',
                'href' => $publicDirRelativePath,
                'type' => 'text/css'
            )
        );

        return $this;
    }

    /**
     * Adds 'script' tag that includes js file to the head
     *
     * @param string $publicPath Relative to the 'public' directory path to css file
     *
     * @throws \Core\Exception\SystemException
     *
     * @return $this
     */
    public function addJsFile($publicPath)
    {
        $this->addHeadTag(
            'script',
            '',
            array(
                'rel' => 'stylesheet',
                'src' => $publicPath,
                'type' => 'text/javascript'
            ),
            true
        );

        return $this;
    }
    
    /**
     * Adds text to the page's head tag. You can add anything tj the head by this way
     *
     * @param string $text Text to add
     *
     * @return $this
     */
    public function addHeadRandomText($text)
    {
        $this->headTags[] = $text;

        return $this;
    }

}