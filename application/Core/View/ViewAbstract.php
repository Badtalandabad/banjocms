<?php

namespace Core\View;

use Core\ModuleElementNameTrait;
use Core\ViewPatch\ViewPatchHandler;

/**
 * Class ViewAbstract
 * Abstract declaration of the common view functionality
 * Bind data to the view in prepareTemplate()
 * for declaration of variables which would be in scope when template's output is running
 *
 * @package Core\View
 */
abstract class ViewAbstract
{
    use ModuleElementNameTrait;

    /**
     * Template for output
     *
     * @var string
     */
    private $template = 'default';

    private $document;

    /**
     * Sets template name
     *
     * @param   string  $template   Name of the template
     */
    protected function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * Returns template name
     *
     * @return string
     */
    protected function getTemplate()
    {
        return $this->template;
    }

    /**
     * Returns path to the template file
     *
     * @return string
     */
    private function getTemplatePath()
    {
        $moduleName = $this->getModuleName();

        $templatePath =  UNITS_PATH . '/' . UNIT_NAME . '/Modules/' . $moduleName
            . '/Views/' . $this->getName() . '/' . $this->getTemplate() . '.php';

        return $templatePath;
    }

    /**
     * Prints output
     *
     * @param string $layoutPath Layout path
     */
    private function printLayout($layoutPath = null)
    {
        if (!$layoutPath) {
            $layoutPath = TEMPLATE_PATH . '/template.phtml';
        }
        require $layoutPath;
    }

    /**
     * Prepares and returns variables which would be
     * in scope when template's output is running
     */
    protected function prepareTemplate()
    {
        // It should be empty or overridden
    }

    /**
     * Saves template output in the buffer,
     * pastes it into the layout and prints layout
     *
     * @param   string  $template   View template
     * @param   string  $layoutPath Layout path
     *
     * @throws  \Core\Exception\SystemException  if template file does not exist
     */
    public function display($template, $layoutPath = null)
    {
        $this->setTemplate($template);

        $this->prepareTemplate();

        $viewPatchHandler = new ViewPatchHandler();
        $viewPatchHandler->processViewPatches();

        $this->document = new Document();
        $this->document->setRenderedViewPatches($viewPatchHandler->getRenderedViewPatches());

        ob_start();

        if (is_file($this->getTemplatePath())) {
            require $this->getTemplatePath();
        } else {
            $exceptionMessage = 'Template file "' . $this->getTemplatePath() . '" does not exist!';
            throw new \Core\Exception\SystemException($exceptionMessage);
        }

        $content = ob_get_contents();
        ob_end_clean();

        $this->document->setContent($content);
        $this->printLayout($layoutPath);

        \SReporter::removeMessages();
    }

}