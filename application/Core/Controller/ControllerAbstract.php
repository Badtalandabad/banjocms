<?php

namespace Core\Controller;

use Core\ModuleElementNameTrait;

/**
 * Class ControllerAbstract
 *
 * @package Core\Controller
 */
abstract class ControllerAbstract
{
    use ModuleElementNameTrait;

    /**
     * Default view. If empty, module name is used as default view name
     *
     * @var string
     */
    protected $defaultView;

    /**
     * Default template. If empty, word "default" is used as default template name
     *
     * @var string
     */
    protected $defaultTemplate;

    /**
     * Default path to layout file. If empty, TEMPLATE_PATH constant + /template.phtml is used
     *
     * @var string
     */
    protected $defaultLayoutPath;

    /**
     * Task to display content
     */
    public function displayTask()
    {
        $view = \SRequester::getGetParam('view');
        $template = \SRequester::getGetParam('template');

        $this->display($view, $template);
    }

    /**
     * Displays content
     *
     * @param string|null $viewName     View name
     * @param string|null $templateName Template name
     * @param string|null $layoutPath   Full path to layout template file
     */
    protected function display($viewName = null, $templateName = null, $layoutPath = null)
    {
        $moduleName = $this->getModuleName();

        if (!$viewName) {
            $viewName = $this->defaultView;
        }
        if (!$viewName) {
            $viewName = $moduleName;
        }
        $viewName = ucfirst($viewName);

        if (!$templateName) {
            $templateName = $this->defaultTemplate;
        }
        if (!$templateName) {
            $templateName = 'default';
        }

        if (!$layoutPath) {
            $layoutPath = $this->defaultLayoutPath;
        }

        $view = \SFactory::getModuleView($viewName, $moduleName);

        $view->display($templateName, $layoutPath);
    }

}