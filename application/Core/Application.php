<?php

namespace Core;

/**
 * Class Application
 * Main application class
 * Takes task from the url and executes needed operations
 */
class Application
{
    /**
     * Constructor
     */
    public function __construct()
    {
        //\SAuth::init();
        $this->init();
    }

    public function init()
    {
        $developmentMode = \SConfig::getParameter(\SConfig::DEVELOPMENT_MODE);
        $showErrors = \SConfig::getParameter(\SConfig::SHOW_PHP_ERRORS);
        if ($developmentMode && $showErrors) {
            ini_set('error_reporting', E_ALL);
            ini_set('display_startup_errors', 1);
            ini_set('html_errors', 1);
            ini_set('display_errors', 1);
        }

        \SReporter::init();
    }

    /**
     * Run the application
     *
     * @throws  \Exception                  If cannot find controller file
     * @throws  Exception\SystemException   If wrong task was given
     */
    public function go()
    {
        $controllerName = UNIT_NAME . '\\'
            . 'Modules\\' . \SRegistry::getModuleName()
            . '\\Controllers\\' . \SRegistry::getControllerName() . 'Controller';

        $task = \SRegistry::getTask();
        $taskMethod = $task . 'Task';

        if (!\SLoader::findClassFilePath($controllerName)) {
            $message = 'Class file was not found. Class name: ' . $controllerName;
            $exception = new \Exception($message, 1);
            //$exception->setSearchPaths(\SLoader::getSearchPaths($controllerName));
            throw $exception;
        }

        $controller = new $controllerName();

        if (!method_exists( $controller, $taskMethod)) {
            $exceptionMessage = 'Route error. There is no task "' . $task . '".';
            throw new Exception\SystemException($exceptionMessage);
        }

        $controller->$taskMethod();
    }

}