<?php
/*
 * Defines base constants
 * */

define('BASE_PATH', dirname(dirname(dirname(__DIR__))));

define('APP_PATH_RELATIVE', 'application');
define('APP_PATH', BASE_PATH . '/' . APP_PATH_RELATIVE);

define('LOG_DIR', BASE_PATH . '/logs');
define('TEMP_DIR', BASE_PATH . '/tmp');

define('SERVICES_PATH_RELATIVE', APP_PATH_RELATIVE . '/Services');
define('SERVICES_PATH', BASE_PATH . '/' . SERVICES_PATH_RELATIVE);

define('TEMPLATE_DIR', 'Templates');

define('PUBLIC_PATH', BASE_PATH . '/public');

define('MODULES_DIR', 'Modules');

define('UNITS_PATH_RELATIVE', APP_PATH_RELATIVE . '/Units');
define('UNITS_PATH', BASE_PATH . '/' . UNITS_PATH_RELATIVE);
define('DEFAULT_UNIT', 'App');

define('DEFAULT_CONTROLLER', 'Base');
define('DISPLAY_TASK', 'display');
define('DEFAULT_TASK', DISPLAY_TASK);

