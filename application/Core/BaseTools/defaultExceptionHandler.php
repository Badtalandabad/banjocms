<?php
/**
 * Default exception handler
 */

/** @var \Exception $exception */
$defaultExceptionHandler = function($exception) {
    ob_end_clean();
    $message = $exception->getMessage();
    if (method_exists($exception, 'getHtmlState') && !$exception->getHtmlState()) {
        $message = htmlspecialchars($message);
    }

    $mainFile = htmlspecialchars($exception->getFile());
    $line = (int)$exception->getLine();
    $trace = str_replace('<br />', '</li><li>', nl2br($exception->getTraceAsString()));
    $scriptPattern = 'var xhr = new XMLHttpRequest();'
        . 'xhr.open(\'GET\', \'http://localhost:63342/api/file?file=__path__&line=__line__\', true);'
        . 'xhr.send();'
        . 'return false;';

    if (defined('BASE_PATH')) {
        // open file in idea IDE
        $mainFileRelativePath = str_replace(BASE_PATH, '', $mainFile);
        $mainFileScript = str_replace('__path__', $mainFileRelativePath, $scriptPattern);
        $mainFileScript = str_replace('__line__', $line, $mainFileScript);

        $mainFile = '<a href="#" onclick="' . $mainFileScript . '">' . $mainFile . '</a>';

        foreach ($exception->getTrace() as $call) {
            $relativePath = str_replace(BASE_PATH, '', $call['file']);
            $script = str_replace('__path__', $relativePath, $scriptPattern);
            $script = str_replace('__line__', $call['line'], $script);
            $fileLink = '<a href="#" onclick="' . $script . '">' . $call['file'] . '</a>';

            $trace = str_replace($call['file'], $fileLink, $trace);
        }
    }



    echo '<head>';
    if (defined('PUBLIC_PATH') && file_exists(PUBLIC_PATH . '/css/base.css')) {
        echo '<link rel="stylesheet" href="/css/base.css" type="text/css" />';
    }
    echo '</head>';
    echo '<body class="error-page">';
    echo '<div class="wrapper">';
    echo '<div class="b-content">';
    echo '<strong>Fatal error.</strong><br>' . $message;
    echo '<br><strong>File:</strong> ' . $mainFile;
    echo '<br><strong>Line:</strong> ' . $line;
    echo '<br><strong>Traceback:</strong> ';
    echo '<ul style="list-style: circle;"><li>';
    echo $trace;
    echo '</li></ul>';
    echo '<br /><a href="/">Go home.</a>';
    echo '</div>';
    echo '</div>';
    echo '</body>';
};

set_exception_handler($defaultExceptionHandler);