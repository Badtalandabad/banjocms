<?php

/**
 * Autoload function
 *
 * @param string $className Full (with the namespaces) name of the class
 *
 * @return bool
 */
$autoload = function($className) {
    return \SLoader::load($className);
};

spl_autoload_register($autoload);
