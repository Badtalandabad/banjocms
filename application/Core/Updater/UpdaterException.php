<?php

namespace Core\Updater;

use Core\Exception\NonFatalException;

/**
 * Class UpdaterException
 *
 * @package Exception\Updater
 */
class UpdaterException extends NonFatalException
{
    /**
     * Initialisation
     */
    public function init()
    {
        $this->message = 'Updater error: ' . $this->message;
    }
} 