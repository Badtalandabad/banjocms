<?php

namespace Core\Updater;

use Core\Exception\NonFatalException;


/**
 * Class RequestFailException
 *
 * @package Exception\Updater
 */
class RequestFailException extends NonFatalException
{
    /**
     * Request name
     *
     * @var string
     */
    protected $_requestName;

    /**
     * @param $requestName
     */
    public function setRequestName( $requestName )
    {
        $this->_requestName = $requestName;
    }

    /**
     * @return mixed
     */
    public function getRequestName()
    {
        return $this->_requestName;
    }

    /**
     *
     */
    public function init()
    {
        if ( $this->_requestName ) {
            $requestName = ' (' . $this->_requestName . ')';
        } else {
            $requestName = '';
        }

        $this->message = 'Request' . $requestName . ' to server failed: '
            . $this->message;
    }
}