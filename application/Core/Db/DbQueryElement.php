<?php

namespace Core\Db;

/**
 * Class DbQueryElement
 *
 * @package Core\Db
 */
class DbQueryElement
{
    const TYPE_SELECT   = 'select';
    const TYPE_INSERT   = 'insert';
    const TYPE_UPDATE   = 'update';
    const TYPE_DELETE   = 'delete';
    const TYPE_TRUNCATE = 'truncate';
    const TYPE_FROM     = 'from';
    const TYPE_JOIN     = 'join';
    const TYPE_WHERE    = 'where';
    const TYPE_GROUP_BY = 'groupby';
    const TYPE_ORDER_BY = 'orderby';
    const TYPE_LIMIT    = 'limit';

    const PARAM_JOIN_TYPE  = 'joinType';
    const PARAM_TABLE      = 'table';
    const PARAM_CONDITION  = 'condition';
    const PARAM_CONDITIONS = 'conditions';
    const PARAM_GLUE       = 'glue';
    const PARAM_VALUES     = 'values';
    const PARAM_COLUMNS    = 'columns';
    const PARAM_SETS       = 'sets';
    const PARAM_LIMIT      = 'limit';
    const PARAM_SORTING    = 'sorting';

    /**
     * List of the priorities of different types of elements
     *
     * @var array
     */
    protected static $priorities = array(
        self::TYPE_SELECT   => 1,
        self::TYPE_INSERT   => 1,
        self::TYPE_UPDATE   => 1,
        self::TYPE_DELETE   => 1,
        self::TYPE_TRUNCATE => 1,
        self::TYPE_FROM     => 2,
        self::TYPE_JOIN     => 3,
        self::TYPE_WHERE    => 4,
        self::TYPE_GROUP_BY => 5,
        self::TYPE_ORDER_BY => 6,
        self::TYPE_LIMIT    => 7,
    );

    /**
     * Element type(select, insert, etc.)
     *
     * @var string
     */
    protected $type;

    /**
     * Element parameters(columns, table, insert values, etc.)
     *
     * @var array
     */
    protected $parameters = [];

    /**
     * Element priority
     *
     * @var int
     */
    protected $priority;

    /**
     * Constructor
     *
     * @param   string  $type   Element type(select, insert, etc.)
     *
     * @throws  \Core\Exception\SystemException  If unknown element type was given.
     */
    public function __construct($type)
    {
        if (!array_key_exists($type, self::$priorities)) {
            $message = 'Unknown query element type: "' . $type . '".';
            throw new \Core\Exception\SystemException($message);
        }

        $this->type = $type;
        $this->priority = self::$priorities[$type];
    }

    /**
     * Returns parameter by name
     *
     * @param   string  $name   Parameter name
     *
     * @return  mixed
     */
    protected function getParam($name)
    {
        if (array_key_exists($name, $this->parameters)) {
            return $this->parameters[$name];
        }

        return null;
    }

    /**
     * Returns element priority
     *
     * @return  int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Returns element type
     *
     * @return  string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets parameter
     *
     * @param   string  $name   Parameter name
     * @param   mixed   $param  Parameter value
     *
     * @return  $this
     */
    public function setParameter($name, $param)
    {
        $this->parameters[$name] = $param;

        return $this;
    }

    /**
     * Returns element parameter. Public version of getParam() method
     *
     * @param   string  $name   Parameter name
     *
     * @return  mixed
     */
    public function getRawParameter($name)
    {
        return $this->getParam($name);
    }

    public function isAppendable()
    {
        if ($this->type == self::TYPE_JOIN) {
            return true;
        }

        return false;
    }
    
    /**
     * Brings parameter to sql query markup. Returns it.
     *
     * @param   string  $name   Parameter name
     *
     * @throws  \Core\Exception\SystemException   If wrong parameter name was given
     *
     * @return  string
     */
    public function paramToSql($name)
    {
        $type = $this->getType();

        switch ($name) {
            case DbQueryElement::PARAM_VALUES:
            case DbQueryElement::PARAM_COLUMNS:
            case DbQueryElement::PARAM_TABLE:
                $param = $this->getParam($name);
                if (is_array($param)) {
                    $param = implode(', ', $param);
                }
                $param = (string)$param;
                break;
            case  DbQueryElement::PARAM_SETS:
                if ($type == self::TYPE_UPDATE) {
                    $param = '';
                    foreach ($this->getParam($name) as $column => $value) {
                        $param .= $column . '=' . $value . ', ';
                    }
                    $param = substr($param, 0, -2);
                }
                break;
            case  DbQueryElement::PARAM_CONDITIONS:
                if ($type == self::TYPE_WHERE) {
                    $glue = ') ' . $this->getParam(DbQueryElement::PARAM_GLUE) . ' (';
                    $conditions = $this->getParam($name);
                    $param = '(' . implode($glue, $conditions) . ')';
                }
                break;
            default:
                /* "limit", "table", "sorting", etc. for
                   "insert", "update", "from", "limit", "truncate", etc. */
                $param = (string)$this->getParam($name);
        }

        if (!isset($param)) {
            //Suggestion: make new exception type
            $message = 'There is no parameter with a name: "' . $name . '".';
            throw new \Core\Exception\SystemException($message);
        }

        return $param;
    }

}
