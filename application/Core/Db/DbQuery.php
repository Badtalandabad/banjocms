<?php

namespace Core\Db;
use Core\Exception\NonFatalException;


/**
 * Class DbQuery
 * Query building mechanism
 *
 * @package Core\Db
 */
class DbQuery
{
    const JOIN_TYPE_LEFT  = 'left';
    const JOIN_TYPE_RIGHT = 'right';
    const JOIN_TYPE_INNER = 'inner';
    const JOIN_TYPE_OUTER = 'outer';

    /**
     * Complete SQL query by string
     *
     * @var string
     */
    private $strQuery = '';

    /**
     * SQL query elements by string
     * Structure like: array(
     *                     priority1 => $part1
     *                     priority2 => $part2
     *                     priority3 => $part3
     *                     ...
     *                 );
     *
     * @var array
     */
    private $strQueryElements = [];

    /**
     * SQL query apart by DbQueryElement instances
     *
     * @var array
     */
    private $queryElements = [];

    /**
     * Adds query element to $_queryElements field
     *
     * @param   DbQueryElement  $element    Query element
     */
    private function addElement($element)
    {
        $this->queryElements[] = $element;
    }

    /**
     * @param       $variable
     * @param array $validTypes
     *
     * @return array
     * @throws \Core\Exception\SystemException
     *
     * @deprecated
     */
    private function wrapParam($variable, $validTypes = [])
    {
        $type = gettype($variable);
        if (!empty($validTypes) && ! in_array($type, $validTypes)) {
            $message = 'Invalid argument type: ' . $type . '.'
                . ' Valid types in this method: ' . implode(', ', $validTypes) . '. ';

            throw new \Core\Exception\SystemException($message);
        }
        switch ($type) {
            case 'string':
                $returnVar = [$variable];
                break;
            case 'array':
                $returnVar = $variable;
                break;
            default:
                $message = 'Unsupported variable type: ' . $type;
                throw new \Core\Exception\SystemException($message);
        }

        return $returnVar;
    }

    /**
     * Adds SELECT operator to query
     * Gets one or more column names, or one or more arrays with column names.
     *
     * @param   string|array    $column Column name or array with column names
     *
     * @throws \Core\Exception\SystemException If type of one of parameters
     *                                         is not string or array
     *
     * @return $this
     */
    public function select($column = '*')
    {
        $element = new DbQueryElement('select');

        $columns = array();

        $args = func_get_args();
        if (!$args && $column) {
            $columns[] = $column;
        }

        foreach ($args as $number => $arg) {
            $type = gettype($arg);
            switch ( $type ) {
                case 'array':
                    $columns = array_merge($columns, $arg);
                    break;
                case 'string':
                    $columns[] = $arg;
                    break;
                default://Suggestion: it should be different type of exception
                    $message = '"' . gettype($arg) . '"(' . ($number + 1)
                        . ' number in list) is given instead of "string" or "array"';
                    throw new \Core\Exception\SystemException($message);
            }
        }

        $element->setParameter('columns', $columns);

        $this->addElement($element);

        return $this;
    }

    /**
     * Adds INSERT operator to query
     *
     * @param   string  $table              Table name
     * @param   array   $columnsAndValues   Array, keys stand for column
     *                                      names, values stand for column values
     * @param   bool    $escape             Escape values or not
     * @param   bool    $quote              Quote values or not
     *
     * @return  $this
     */
    public function insert($table, $columnsAndValues, $escape = true, $quote = true)
    {
        $db = \SFactory::getDb();
        $table = $escape ? $db->escape($table) : $table;
        $table = $quote ? $db->qn($table, false) : $table;
        // if escape and quote both are false nothing happens
        $columnsAndValues = $db->tidySets($columnsAndValues, $escape, $quote, $escape, $quote);

        $element = new DbQueryElement('insert');

        $element->setParameter('table', $table);

        $element->setParameter('columns', array_keys($columnsAndValues));
        $element->setParameter('values', array_values($columnsAndValues));

        $this->addElement($element);

        return $this;
    }

    /**
     * Adds UPDATE operator to query
     *
     * @param   string  $table              Table name
     * @param   array   $columnsAndValues   Array, keys stand for column
     *                                      names, values stand for column values
     * @param   bool    $escape             Escape values or not
     * @param   bool    $quote              Quote values or not
     *
     * @return  $this
     */
    public function update($table, $columnsAndValues, $escape = true, $quote = true)
    {
        $db = \SFactory::getDb();
        $table = $escape ? $db->escape($table) : $table;
        $table = $quote ? $db->qn($table, false) : $table;
        // if escape and quote both are false nothing happens
        $columnsAndValues = $db->tidySets($columnsAndValues, $escape, $quote, $escape, $quote);

        $element = new DbQueryElement(DbQueryElement::TYPE_UPDATE);

        $element->setParameter(DbQueryElement::PARAM_TABLE, $table);
        $element->setParameter(DbQueryElement::PARAM_SETS, $columnsAndValues);

        $this->addElement($element);

        return $this;
    }

    /**
     * Adds DELETE operator to query
     *
     * @return  $this
     */
    public function delete()
    {
        $this->queryElements[] = new DbQueryElement(DbQueryElement::TYPE_DELETE);

        return $this;
    }

    /**
     * Adds FROM operator to query
     *
     * @param string|array $table  Table name
     * @param bool         $escape Escape table or not
     * @param bool         $quote  Quote table or not
     *
     * @return  $this
     */
    public function from($table, $escape = true, $quote = true)
    {
        $db = \SFactory::getDb();
        $table = $escape ? $db->escape($table) : $table;
        $table = $quote ? $db->qn($table, false) : $table;

        $element = new DbQueryElement(DbQueryElement::TYPE_FROM);

        $element->setParameter(DbQueryElement::PARAM_TABLE, $table);

        $this->addElement($element);

        return $this;
    }

    /**
     * Left join
     *
     * @param string $tableName Table name
     * @param string $condition ON condition
     *
     * @return DbQuery
     */
    public function leftJoin($tableName, $condition)
    {
        return $this->join(self::JOIN_TYPE_LEFT, $tableName, $condition);
    }

    /**
     * Right join
     *
     * @param string $tableName Table name
     * @param string $condition ON condition
     *
     * @return DbQuery
     */
    public function rightJoin($tableName, $condition)
    {
        return $this->join(self::JOIN_TYPE_RIGHT, $tableName, $condition);
    }

    /**
     * Inner join
     *
     * @param string $tableName Table name
     * @param string $condition ON condition
     *
     * @return DbQuery
     */
    public function innerJoin($tableName, $condition)
    {
        return $this->join(self::JOIN_TYPE_INNER, $tableName, $condition);
    }

    /**
     * Outer join
     *
     * @param string $tableName Table name
     * @param string $condition ON condition
     *
     * @return DbQuery
     */
    public function outerJoin($tableName, $condition)
    {
        return $this->join(self::JOIN_TYPE_OUTER, $tableName, $condition);
    }

    /**
     * Join
     *
     * @param string $joinType  Join type
     * @param string $tableName Table name
     * @param string $condition ON condition
     *
     * @return DbQuery
     */
    public function join($joinType, $tableName, $condition)
    {
        $element = new DbQueryElement(DbQueryElement::TYPE_JOIN);

        $element->setParameter(DbQueryElement::PARAM_JOIN_TYPE, $joinType);
        $element->setParameter(DbQueryElement::PARAM_TABLE, $tableName);
        $element->setParameter(DbQueryElement::PARAM_CONDITION, $condition);

        $this->addElement($element);

        return $this;
    }

    /**
     * Adds WHERE operator to query. Gets one or more conditions
     * or one or more arrays with conditions and glues all conditions
     * by AND operator
     *
     * @param   string|array    $condition  String with "where" condition
     *                                      or array with them
     *
     * @return  $this
     */
    public function whereAnd($condition)
    {
        $conditions = func_get_args();
        return $this->setWhere($conditions, 'AND');
    }

    /**
     * Adds WHERE operator to query. Gets one or more conditions
     * or one or more arrays with conditions and glues all conditions
     * by OR operator
     *
     * @param   string|array    $condition  String with "where" condition
     *                                      or array with them
     *
     * @return  $this
     */
    public function whereOr($condition)
    {
        $conditions = func_get_args();
        return $this->setWhere($conditions, 'OR');
    }

    /**
     * Adds WHERE operator to query. Gets one condition
     * or array with conditions
     *
     * @param   string|array    $condition  String with "where" condition
     *                                      or array with such strings
     * @param   string          $glue       operator(AND, OR) which will
     *                                      be between conditions
     *
     * @throws  \Core\Exception\SystemException  If type of an argument is not valid
     *
     * @return  $this
     */
    public function where($condition, $glue = 'AND')
    {
        $validTypes = ['string', 'array'];
        $conditions = $this->wrapParam($condition, $validTypes);

        return $this->setWhere($conditions, $glue);
    }

    /**
     * Adds "where" element to query. Contains common functionality
     * of different "where" methods. Gets array of conditions
     * and operator(AND, OR) which will be between conditions
     *
     * @param   array   $conditions "where" conditions
     * @param   string  $glue       operator(AND, OR) which will
     *                              be between conditions
     *
     * @return  $this
     */
    protected function setWhere($conditions, $glue)
    {
        $element = new DbQueryElement(DbQueryElement::TYPE_WHERE);

        $element->setParameter(DbQueryElement::PARAM_CONDITIONS, $conditions);
        $element->setParameter(DbQueryElement::PARAM_GLUE, $glue);

        $this->addElement($element);

        return $this;
    }

    /**
     * Adds LIMIT operator to query
     *
     * @param   int $offset Offset of "limit clause"
     * @param   int $limit  "limit" clause
     *
     * @return  $this
     */
    public function limit($offset, $limit)
    {
        $element = new DbQueryElement(DbQueryElement::TYPE_LIMIT);

        $offset = (int)$offset;
        $limit  = (int)$limit;

        //it is not necessary at all
        if ($offset) {
            $limit = $offset . ', ' . $limit;
        }

        $element->setParameter('limit', $limit);

        $this->addElement($element);

        return $this;
    }

    /**
     * Adds GROUP BY operator to query
     *
     * @param string|array $column Column(s) to sort by
     * @param bool         $escape Escape column or not
     * @param bool         $quote  Quote column or not
     *
     * @return $this
     */
    public function groupBy($column, $escape = true, $quote = true)
    {
        $db = \SFactory::getDb();
        $column = $escape ? $db->escape($column) : $column;
        $column = $quote ? $db->qn($column, false) : $column;

        $element = new DbQueryElement(DbQueryElement::TYPE_GROUP_BY);

        $element->setParameter(DbQueryElement::PARAM_COLUMNS, $column);

        $this->addElement($element);

        return $this;
    }

    /**
     * Adds ORDER BY operator to query
     *
     * @param string|array $column  Column(s) to sort by
     * @param string       $sorting Sorting order
     * @param bool         $escape  Escape column or not
     * @param bool         $quote   Quote column or not
     *
     * @return $this
     */
    public function orderBy($column, $sorting = 'ASC', $escape = true, $quote = true)
    {
        $db = \SFactory::getDb();
        $column = $escape ? $db->escape($column) : $column;
        $column = $quote ? $db->qn($column, false) : $column;

        $element = new DbQueryElement(DbQueryElement::TYPE_ORDER_BY);

        $sorting = strtoupper($sorting);
        if ($sorting != 'ASC' && $sorting != 'DESC') {
            $sorting = 'ASC';
        }

        $element->setParameter(DbQueryElement::PARAM_COLUMNS, $column);
        $element->setParameter(DbQueryElement::PARAM_SORTING, $sorting);

        $this->addElement($element);

        return $this;
    }

    /**
     * Adds TRUNCATE operator to query
     *
     * @param   string  $table  Table name
     *
     * @return  $this
     */
    public function truncate($table)
    {
        $element = new DbQueryElement(DbQueryElement::TYPE_TRUNCATE);

        $element->setParameter(DbQueryElement::PARAM_TABLE, $table);

        $this->addElement($element);

        return $this;
    }

    /**
     * Builds sql query. Returns sql string
     *
     * @throws \Core\Exception\SystemException If type of the query element
     *                                         is not appropriate or is not supported
     *
     * @return string
     *
     * @throws NonFatalException Array of tables was given to update statement
     */
    public function buildQuery()
    {
        $this->strQuery = '';

        /** @var DbQueryElement $element */
        foreach ($this->queryElements as $element) {
            switch ($element->getType()) {
                case DbQueryElement::TYPE_SELECT:
                    $sql = 'SELECT ' . $element->paramToSql(DbQueryElement::PARAM_COLUMNS) . ' ';
                    break;
                case DbQueryElement::TYPE_INSERT:
                    $sql = 'INSERT INTO ' . $element->paramToSql(DbQueryElement::PARAM_TABLE)
                        . ' (' . $element->paramToSql(DbQueryElement::PARAM_COLUMNS) . ') '
                        . ' VALUES (' . $element->paramToSql(DbQueryElement::PARAM_VALUES) . ') ';
                    break;
                case DbQueryElement::TYPE_UPDATE:
                    if (is_array($element->getRawParameter(DbQueryElement::PARAM_TABLE))) {
                        throw new NonFatalException('Multiple tables in update statement');
                    }
                    $sql = 'UPDATE ' . $element->paramToSql(DbQueryElement::PARAM_TABLE)
                        . ' SET ' . $element->paramToSql(DbQueryElement::PARAM_SETS);
                    break;
                case DbQueryElement::TYPE_DELETE:
                    $sql = 'DELETE ';
                    break;
                case DbQueryElement::TYPE_TRUNCATE:
                    $sql = 'TRUNCATE TABLE ' . $element->paramToSql(DbQueryElement::PARAM_TABLE);
                    break;
                case DbQueryElement::TYPE_FROM:
                    $sql = ' FROM ' . $element->paramToSql(DbQueryElement::PARAM_TABLE) . ' ';
                    break;
                case DbQueryElement::TYPE_JOIN:
                    $sql = ' ' . strtoupper($element->paramToSql(DbQueryElement::PARAM_JOIN_TYPE))
                        . ' JOIN ' . $element->paramToSql(DbQueryElement::PARAM_TABLE) . ' ON '
                        . $element->paramToSql(DbQueryElement::PARAM_CONDITION);
                    break;
                case DbQueryElement::TYPE_WHERE:
                    $sql = ' WHERE ' . $element->paramToSql(DbQueryElement::PARAM_CONDITIONS) . ' ';
                    break;
                case  DbQueryElement::TYPE_LIMIT:
                    $sql = ' LIMIT ' . $element->paramToSql(DbQueryElement::PARAM_LIMIT) . ' ';
                    break;
                case  DbQueryElement::TYPE_GROUP_BY:
                    $sql = ' GROUP BY ' . $element->paramToSql(DbQueryElement::PARAM_COLUMNS);
                    break;
                case  DbQueryElement::TYPE_ORDER_BY:
                    $sql = ' ORDER BY ' . $element->paramToSql(DbQueryElement::PARAM_COLUMNS)
                        . ' ' . $element->paramToSql(DbQueryElement::PARAM_SORTING);
                    break;
                default:
                    throw new \Core\Exception\SystemException('Unknown db query element type');
            }
            $priority = $element->getPriority();
            if (!empty($this->strQueryElements[$priority]) && $element->isAppendable()) {
                $this->strQueryElements[$priority] .= $sql;
            } else {
                $this->strQueryElements[$priority] = $sql;
            }
        }
        ksort($this->strQueryElements);
        $this->strQuery = implode($this->strQueryElements);
        return $this->strQuery;
    }

} 