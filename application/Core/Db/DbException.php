<?php

namespace Core\Db;

use Core\Exception\NonFatalException;

/**
 * Database exception
 *
 * @package Exception
 */
class DbException extends NonFatalException
{
    /**
     * Initialisation
     */
    public function init()
    {
        $this->message = 'Database error: ' . $this->message;
    }
}
