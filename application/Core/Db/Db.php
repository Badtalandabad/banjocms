<?php

namespace Core\Db;


/**
 * Class Db
 * Database control class. Singletone.
 *
 * @package Core\Db
 */
class Db
{
    /**
     * Single instance of the object
     *
     * @var self
     */
    private static $instance;

    /**
     * Database driver object
     *
     * @var Drivers\DbDriverAbstract
     */
    private $driver;

    /**
     * Returns single instance of the object
     *
     * @return Db
     */
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone() {}
    private function __wakeup() {}

    /**
     * Constructor
     */
    protected function __construct()
    {
        $dbDriverName = \SConfig::getParameter(\SConfig::DB_DRIVER);

        $this->connect($dbDriverName);
        //TODO: add result check
    }

    /**
     * Sets database driver object
     *
     * @param Drivers\DbDriverAbstract $driver Database driver object
     *
     * @return $this
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Returns database driver object
     *
     * @return  Drivers\DbDriverAbstract
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Sets quote symbol. Which using in quote() and q() methods
     *
     * @param   string  $quote  Quote symbol
     *
     * @return  $this
     */
    public function setQuote($quote)
    {
        $this->getDriver()
            ->setQuote($quote);

        return $this;
    }

    /**
     * Returns quote symbol. Which using in quote() and q() methods
     *
     * @return  string
     */
    public function getQuote()
    {
        return $this->getDriver()
            ->getQuote();
    }

    /**
     * Sets quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @param   string  $quoteForNames  Quote symbol
     *
     * @return  $this
     */
    public function setQuoteForNames($quoteForNames)
    {
        $this->getDriver()
            ->setQuoteForNames($quoteForNames);

        return $this;
    }

    /**
     * Returns quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @return  string
     */
    public function getQuoteForNames()
    {
        return $this->getDriver()
            ->getQuoteForNames();
    }

    /**
     * Returns driver class name
     *
     * @param   string  $driverName Driver name
     *
     * @return  string
     */
    public function getDriverClassName($driverName)
    {
        $dbDriverClass = '\\' . __NAMESPACE__ . '\\Drivers\\'
            . ucfirst(strtolower($driverName));

        return $dbDriverClass;
    }

    /**
     * Returns driver result class name
     *
     * @param   string  $driverName Driver name
     *
     * @return  string
     */
    public function getDriverResultClassName($driverName)
    {
        $dbDriverResultClass = $this->getDriverClassName($driverName) . 'Result';

        return $dbDriverResultClass;
    }

    /**
     * Creates db driver instance and connects to the database
     *
     * @param string $driverName Db driver name
     *
     * @return bool
     */
    public function connect($driverName)
    {
        $dbDriverClass = $this->getDriverClassName($driverName);

        if (!class_exists($dbDriverClass)) {
            return false;
        }

        $dbHost     = \SConfig::getParameter(\SConfig::DB_HOST);
        $dbUser     = \SConfig::getParameter(\SConfig::DB_USER);
        $dbPassword = \SConfig::getParameter(\SConfig::DB_PASSWORD);
        $dbCharset  = \SConfig::getParameter(\SConfig::DB_CHARSET);
        $dbName     = \SConfig::getParameter(\SConfig::DB_NAME);

        /** @var Drivers\DbDriverAbstract $dbDriver */
        $dbDriver = new $dbDriverClass(
            $this,
            $dbHost,
            $dbUser,
            $dbPassword,
            $dbCharset,
            $dbName
        );

        $this->setDriver($dbDriver);

        $dbDriver->connect();

        return true;
    }

    /**
     * Executes sql query
     *
     * @param string|DbQuery $query Query to execute
     *
     * @return bool|\Core\Db\DbQueryResultHandler
     */
    public function execute($query)
    {
        $driver = $this->getDriver();

        $query = $driver->prepareQuery($query);

        $resultResource = $driver->execute($query);
        if (!$resultResource) {
            \SLogger::writeLog('Database error: ' . $this->driver->getError());
            return $resultResource;
        }

        $queryResultHandler = new DbQueryResultHandler($this, $resultResource);

        return $queryResultHandler;
    }

    public function getError()
    {
        $this->driver->getError();
    }
    
    /**
     * Returns DbQuery object for building a query
     *
     * @return DbQuery
     */
    public function getQuery()
    {
        return new DbQuery();
    }

    /**
     * Quotes and optionally escapes a string(s) to
     * database requirements for insertion into the database
     *
     * @param string|array $value  Text or array of texts to quote
     * @param bool         $escape To escape or not
     *
     * @return string|array
     */
    public function quote($value, $escape = true)
    {
        $driver = $this->getDriver();

        if (is_array($value)) {
            $quotedValue = [];
            foreach ($value as $key => $text) {
                $quotedValue[$key] = $driver->quoteValue($text, $escape);
            }
        } else {
            $quotedValue = $driver->quoteValue($value, $escape);
        }

        return $quotedValue;
    }

    /**
     * Wrap an SQL statement identifier name(s) such as column,
     * table or database names in quotes
     *
     * @param string|array $name   Name or array of names to quote
     * @param bool         $escape To escape or not
     *
     * @return string|array
     */
    public function quoteName($name, $escape = true)
    {
        $driver = $this->getDriver();

        if (is_array($name)) {
            $quotedName = [];
            foreach ($name as $key => $text) {
                $quotedName[$key] = $driver->quoteName($text, $escape);
            }
        } else {
            $quotedName = $driver->quoteName($name, $escape);
        }

        return $quotedName;
    }

    /**
     * Alias for quote() method
     *
     * Quotes and optionally escapes a string(s) to
     * database requirements for insertion into the database
     *
     * @param string|array $text   Text or array of texts to quote
     * @param bool         $escape To escape text or not
     *
     * @return string|array
     */
    public function q($text, $escape = true)
    {
        return $this->quote($text, $escape);
    }

    /**
     * Alias for quoteName() method
     *
     * Wrap an SQL statement identifier name(s) such as column,
     * table or database names in quotes
     *
     * @param string|array $name   Name or array of names to quote
     * @param bool         $escape To escape text or not
     *
     * @return string|array
     */
    public function qn($name, $escape = true)
    {
        return $this->quoteName($name, $escape);
    }

    /**
     * Escapes text for usage in an SQL statement. Uses db driver escaping function.
     * If array given, escapes every entry of array and returns it.
     *
     * @param string|array $value Text or array of texts to escape
     *
     * @return string|array
     */
    public function escape($value)
    {
        $driver = $this->getDriver();

        if (is_array($value)) {
            $escapedValue = [];
            foreach ($value as $key => $text) {
                $escapedValue[$key] = $driver->escapeValue($text);
            }
        } else {
            $escapedValue = $driver->escapeValue($value);
        }

        return $escapedValue;
    }

    /**
     * Quotes and escapes array for insert, update queries. Returns it.
     *
     * @param array $sets         Array of sets, keys are column names, values are setting values.
     * @param bool  $escapeValues To escape values of given array or not before quoting
     * @param bool  $quoteValues  To quote values(with $this->_quote) of given array or not
     * @param bool  $escapeKeys   To escape values of given array or not before quoting
     * @param bool  $quoteKeys    To quote keys(with $this->_quoteForNames) of given array or not
     *
     * @return array
     */
    public function tidySets($sets, $escapeValues = true, $quoteValues = true, $escapeKeys = true, $quoteKeys = true)
    {
        $tidySets = [];
        foreach ($sets as $column => $value) {
            $column = $escapeKeys ? $this->escape($column) : $column;
            $column = $quoteKeys ? $this->qn($column, false) : $column;

            $value = $escapeValues ? $this->escape($value) : $value;
            $value = $quoteValues ? $this->q($value, false) : $value;

            $tidySets[$column] = $value;
        }
        return $tidySets;
    }

    /**
     * Quotes array for select query.  Returns it.
     *
     * @param array $columns Array of column names, which will be selected
     *
     * @return array
     */
    public function quoteColumns($columns)
    {
        foreach ($columns as $key => $column) {
            if (trim($column) == '*') {
                continue;
            }
            $columns[$key] = $this->qn($column);
        }

        return $columns;
    }

}
