<?php

namespace Core\Db\Drivers;


/**
 * Class PostgresqlResult
 *
 * @package Core\Db\Drivers
 */
class PostgresqlResult extends ResultAbstract
{
    /**
     * Query result resource
     *
     * @var resource
     */
    private $_resultResource;

    /**
     * Returns query result resource
     *
     * @return  resource
     */
    public function getResultResource()
    {
        return $this->_resultResource;
    }

    /**
     * Sets query result resource
     *
     * @param   resource    $resultResource Query result resource
     *
     * @return  $this
     */
    public function setResultResource($resultResource)
    {
        $this->_resultResource = $resultResource;
    }

    /**
     * Constructor. Saves query result resource
     *
     * @param   DbDriverAbstract    $driver         Database driver
     * @param   resource            $resultResource Query result resource
     */
    public function __construct($driver, $resultResource)
    {
        $this->setDriver($driver);
        $this->setResultResource($resultResource);
    }

    /**
     * Frees the memory associated with a result
     */
    public function free()
    {
        pg_free_result($this->getResultResource());
    }

    /**
     * Returns names of the columns of the result set
     *
     * @return  array
     */
    public function fetchColumnNames()
    {
        $resultResource = $this->getResultResource();

        $columnNumbers = pg_num_fields($resultResource);

        $columns = [];
        for ($i = 0; $i < $columnNumbers; $i++) {
            $columns[] = pg_field_name($resultResource, $i);
        }

        return $columns;
    }

    /**
     * Returns first value from the first row from result set
     *
     * @return  string|false
     */
    public function fetchValue()
    {
        return pg_fetch_result($this->getResultResource(), 0, 0);
    }

    /**
     * Returns the first row of a result set by object
     * If class name parameter is not specified, a stdClass objects is returned.
     *
     * @param   string  $className  The name of the class to instantiate, set the properties of and return.
     *
     * @return  \stdClass
     */
    public function fetchRowObj($className = 'stdClass')
    {
        return pg_fetch_object($this->getResultResource(), null, $className);
    }

    /**
     * Returns whole result set by objects
     * If class name parameter is not specified, stdClass objects are returned.
     *
     * @param   string  $className  The name of the class to instantiate, set the properties of and return.
     *
     * @return  \stdClass[]
     */
    public function fetchAllObj($className = 'stdClass')
    {
        $objects = [];
        while ($row = pg_fetch_object($this->getResultResource(), null, $className)) {
            $objects[] = $row;
        }

        return $objects;
    }

    /**
     * Returns the first row of a result set by associative array
     *
     * @return  array
     */
    public function fetchRowAssoc()
    {
        return pg_fetch_assoc($this->getResultResource());
    }

    /**
     * Returns whole result set by associative arrays
     *
     * @return  array[]
     */
    public function fetchAllAssoc()
    {
        return pg_fetch_all($this->getResultResource());
    }

    /**
     * Returns result set rows count
     *
     * @return  int
     */
    public function getRowsCount()
    {
        return pg_num_rows($this->getResultResource());
    }

    /**
     * Returns number of affected rows
     *
     * @return  int
     */
    public function getAffectedRowsCount()
    {
        return pg_affected_rows($this->getResultResource());
    }

    /**
     * Returns error message
     *
     * @return  string|false
     */
    public function getErrorMessage()
    {
        return pg_result_error($this->getResultResource());
    }

}