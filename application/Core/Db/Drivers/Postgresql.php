<?php

namespace Core\Db\Drivers;

/**
 * Database PostgreSQL driver
 *
 * @package Core\Db\Drivers
 */
class Postgresql extends DbDriverAbstract
{
    /**
     * Driver name
     *
     * @var string
     */
    protected $name = 'postgresql';

    /**
     * Quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @var string
     */
    protected $quoteForNames = '"';

    /**
     * Quote symbol. Which using in quote() and q() methods
     *
     * @var string
     */
    protected $quote = '\'';

    /**
     * PostgreSQL connection
     *
     * @var resource
     */
    protected $connection;

    /**
     * Returns PostgreSQL connection
     *
     * @return resource
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Sets PostgreSQL connection
     *
     * @param resource $connection PostgreSQL connection
     *
     * @return $this
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;

        return $this;
    }

    /**
     * Connects to the database
     *
     * @throws \Core\Db\DbException If Mysql connection failed
     */
    public function connect()
    {
        $connectCommand = 'host=localhost'
            . ' dbname=' . $this->getDbName()
            . ' user=' . $this->getDbUser()
            . ' password=' . $this->getDbPassword()
            . ' options=\'--client_encoding=' . $this->getDbCharset() . '\'';

        $this->connection = pg_connect($connectCommand);
        //pg_query($this->_connection, 'SET standard_conforming_strings=off');
        //pg_query($this->_connection, 'SET escape_string_warning=off');

        if (!$this->connection) {
            throw new \Core\Db\DbException('Could not connect: ' . pg_last_error());
        }
    }

    /**
     * Wraps an SQL statement identifier name such as column,
     * table or database names in quotes
     *
     * @param string $name   Text to quote
     * @param bool   $escape To escape or not
     *
     * @return string
     */
    public function quoteName($name, $escape = true)
    {
        if ($escape) {
            return $this->escapeName($name);
        }

        $quote = $this->getQuoteForNames();

        $quotedName = $quote . $name . $quote;

        return $quotedName;
    }

    /**
     * Escapes text for usage in an SQL statement.
     *
     * @param string $text Text to escape
     *
     * @return string
     */
    public function escapeValue($text)
    {
        return pg_escape_string($this->getConnection(), $text);
    }

    /**
     * Escapes column name for usage in an SQL statement. Adds quotes
     *
     * @param string $name Name to escape
     *
     * @return string
     */
    public function escapeName($name)
    {
        return pg_escape_identifier($this->getConnection(), $name);
    }

    /**
     * Executes query
     *
     * @param string $query Query to execute
     *
     * @return bool
     */
    public function execute($query)
    {
        return pg_query($this->getConnection(), $query);
    }

    /**
     * Returns db error message
     *
     * @return string
     */
    public function getError()
    {
        return (string)pg_last_error($this->getConnection());
    }

}