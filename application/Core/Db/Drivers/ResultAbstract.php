<?php

namespace Core\Db\Drivers;


abstract class ResultAbstract
{
    /**
     * Database driver
     *
     * @var DbDriverAbstract
     */
    protected $_driver;

    /**
     * Sets driver instance
     *
     * @param DbDriverAbstract $driver Database driver instance
     *
     * @return $this
     */
    protected function setDriver($driver)
    {
        $this->_driver = $driver;

        return $this;
    }

    /**
     * Returns driver instance
     *
     * @return  DbDriverAbstract
     */
    public function getDriver()
    {
        return $this->_driver;
    }

    /**
     * Constructor. Saves query result resource
     *
     * @param   DbDriverAbstract    $driver         Database driver
     * @param   mixed               $resultResource Query result resource
     */
    abstract public function __construct($driver, $resultResource);

    /**
     * Frees the memory associated with a result
     */
    abstract public function free();

    /**
     * Returns names of the columns of the result set
     *
     * @return  array
     */
    abstract public function fetchColumnNames();

    /**
     * Returns first value from the first row from result set
     *
     * @return  string|false
     */
    abstract public function fetchValue();

    /**
     * Returns the first row of a result set by object
     * If class name parameter is not specified, a stdClass object is returned.
     *
     * @param   string  $className  The name of the class to instantiate, set the properties of and return.
     *
     * @return  \stdClass
     */
    abstract public function fetchRowObj($className = 'stdClass');

    /**
     * Returns whole result set by objects
     * If class name parameter is not specified, stdClass objects are returned.
     *
     * @param   string  $className  The name of the class to instantiate, set the properties of and return.
     *
     * @return  \stdClass[]
     */
    abstract public function fetchAllObj($className = 'stdClass');

    /**
     * Returns the first row of a result set by associative array
     *
     * @return  array
     */
    abstract public function fetchRowAssoc();

    /**
     * Returns whole result set by associative arrays
     *
     * @return  array[]
     */
    abstract public function fetchAllAssoc();

    /**
     * Returns result set rows count
     *
     * @return  int
     */
    abstract public function getRowsCount();

    /**
     * Returns error message
     *
     * @return  string
     */
    abstract public function getErrorMessage();

    /**
     * Returns number of affected rows
     *
     * @return  int
     */
    abstract public function getAffectedRowsCount();

}