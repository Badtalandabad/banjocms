<?php

namespace Core\Db\Drivers;


/**
 * Database Mysqli driver
 *
 * @package Core\Db\Drivers
 */
class Mysqli extends DbDriverAbstract
{
    /**
     * Driver name
     *
     * @var string
     */
    protected $name = 'mysqli';

    /**
     * Quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @var string
     */
    protected $quoteForNames = '`';

    /**
     * Quote symbol. Which using in quote() and q() methods
     *
     * @var string
     */
    protected $quote = '\'';

    /**
     * MySQLi object
     *
     * @var \mysqli
     */
    private $mysqli;

    /**
     * Sets mysqli object
     *
     * @param   \mysqli $mysqli Mysqli object
     *
     * @return  $this
     */
    public function setMysqli($mysqli)
    {
        $this->mysqli = $mysqli;

        return $this;
    }

    /**
     * Returns mysqli object
     *
     * @return \mysqli
     */
    public function getMysqli()
    {
        return $this->mysqli;
    }

    /**
     * Connects to the database
     *
     * @throws \Core\Db\DbException If Mysql connection failed
     *
     * @return bool
     */
    public function connect()
    {
        $mysqli = new \mysqli(
            $this->getDbHost(),
            $this->getDbUser(),
            $this->getDbPassword(),
            $this->getDbName()
        );

        $this->setMysqli($mysqli);

        if ($mysqli->connect_errno) {
            $exceptionMessage = 'MySQL connection failure(' . $mysqli->connect_errno
                . ") " . $mysqli->connect_error;
            throw new \Core\Db\DbException($exceptionMessage);
        }

        $this->getMysqli()
            ->set_charset($this->getDbCharset());

        return true;
    }

    /**
     * Executes sql query
     *
     * @param   string  $query  Query to execute
     *
     * @return  bool|\mysqli_result
     */
    public function execute($query)
    {
        return $this->getMysqli()
            ->query($query);
    }

    /**
     * Returns db error message
     *
     * @return string
     */
    public function getError()
    {
        return $this->getMysqli()->error;
    }

    /**
     * Returns last inserted id
     *
     * @return int|null
     */
    public function getLastInsertedId()
    {
        return $this->getMysqli()->insert_id;
    }

    /**
     * Escapes text for usage in an SQL statement.
     * Uses mysqli_real_escape_string.
     *
     * @param   string  $text   Text to escape
     *
     * @return  string
     */
    public function escapeValue($text)
    {
        $escapedText = $this->getMysqli()
            ->real_escape_string($text);

        return $escapedText;
    }

    /**
     * Escapes column name for usage in an SQL statement.
     * Uses mysqli_real_escape_string.
     *
     * @param   string  $name   Name to escape
     *
     * @return  string
     */
    public function escapeName($name)
    {
        return $this->escapeValue($name);
    }

}