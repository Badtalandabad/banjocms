<?php

namespace Core\Db\Drivers;

use Core\Db\DbQuery;


/**
 * Class DbDriver
 *
 * @package Core\Db\Drivers
 */
abstract class DbDriverAbstract
{
    /**
     * Database object
     *
     * @var \Core\Db\Db
     */
    protected $db;

    /**
     * Driver name
     *
     * @var string
     */
    protected $name;

    /**
     * Database hostname.
     *
     * @var string
     */
    protected $dbHost;

    /**
     * Database name.
     *
     * @var string
     */
    protected $dbName;

    /**
     * Database username.
     *
     * @var string
     */
    protected $dbUser;

    /**
     * Database password.
     *
     * @var string
     */
    protected $dbPassword;

    /**
     * Database charset
     *
     * @var string
     */
    protected $dbCharset;

    /**
     * Quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @var string
     */
    protected $quoteForNames;

    /**
     * Quote symbol. Which using in quote() and q() methods
     *
     * @var string
     */
    protected $quote;

    /**
     * Connects to the database
     *
     * @return bool
     */
    abstract public function connect();

    /**
     * Executes query
     *
     * @param string|\Core\Db\DbQuery $query Query to execute
     *
     * @return bool
     */
    abstract public function execute($query);

    /**
     * Returns db error message
     *
     * @return string
     */
    abstract public function getError();

    /**
     * Escapes text for usage in an SQL statement.
     *
     * @param string $text Text to escape
     *
     * @return string
     */
    abstract public function escapeValue($text);

    /**
     * Escapes column name for usage in an SQL statement.
     *
     * @param string $name Name to escape
     *
     * @return string
     */
    abstract public function escapeName($name);

    /**
     * Recommended to implement if db supports such possibility
     * Returns last inserted id
     *
     * @return int|null
     */
    //abstract public function getLastInsertedId();

    /**
     * Sets database object
     *
     * @param \Core\Db\Db $db Database object
     *
     * @return static
     */
    protected function setDb($db) : self
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Returns database object
     *
     * @return \Core\Db\Db
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * Returns driver name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets driver name
     *
     * @param string $name Driver name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Sets database host
     *
     * @param string  $dbHost Database host
     *
     * @return $this
     */
    public function setDbHost($dbHost)
    {
        $this->dbHost = $dbHost;

        return $this;
    }

    /**
     * Returns database host
     *
     * @return string
     */
    public function getDbHost()
    {
        return $this->dbHost;
    }

    /**
     * Sets database name
     *
     * @param  string $dbName Database name
     *
     * @return $this
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;

        return $this;
    }

    /**
     * Returns database name
     *
     * @return string
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * Sets database username
     *
     * @param string $dbUser Database user
     *
     * @return $this
     */
    public function setDbUser($dbUser)
    {
        $this->dbUser = $dbUser;

        return $this;
    }

    /**
     * Returns database username
     *
     * @return string
     */
    public function getDbUser()
    {
        return $this->dbUser;
    }

    /**
     * Sets database password
     *
     * @param string $dbPassword Database user password
     *
     * @return $this
     */
    public function setDbPassword($dbPassword)
    {
        $this->dbPassword = $dbPassword;

        return $this;
    }

    /**
     * Returns database password
     *
     * @return  string
     */
    public function getDbPassword()
    {
        return $this->dbPassword;
    }

    /**
     * Sets database charset
     *
     * @param string $dbCharset Database charset
     *
     * @return $this
     */
    public function setDbCharset($dbCharset)
    {
        $this->dbCharset = $dbCharset;

        return $this;
    }

    /**
     * Returns database charset
     *
     * @return string
     */
    public function getDbCharset()
    {
        return $this->dbCharset;
    }


    /**
     * Sets quote symbol. Which using in quote() and q() methods
     *
     * @param string $quote Quote symbol
     *
     * @return $this
     */
    public function setQuote($quote)
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * Returns quote symbol. Which using in quote() and q() methods
     *
     * @return string
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * Sets quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @param string $quoteForNames Quote symbol
     *
     * @return $this
     */
    public function setQuoteForNames($quoteForNames)
    {
        $this->quoteForNames = $quoteForNames;

        return $this;
    }

    /**
     * Returns quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @return  string
     */
    public function getQuoteForNames()
    {
        return $this->quoteForNames;
    }

    /**
     * Constructor. Connects to the database
     *
     * @param \Core\Db\Db $db         Database object
     * @param string      $dbHost     Database host
     * @param string      $dbUser     Database user
     * @param string      $dbPassword Database password
     * @param string      $dbCharset  Database charset
     * @param string      $dbName     Database name
     *
     * @throws \Core\Db\DbException If MySQL connection failed
     */
    public function __construct($db, $dbHost, $dbUser, $dbPassword, $dbCharset, $dbName)
    {
        $this->setDb($db);

        $this->setDbHost($dbHost);
        $this->setDbUser($dbUser);
        $this->setDbPassword($dbPassword);
        $this->setDbCharset($dbCharset);
        $this->setDbName($dbName);
    }

    /**
     * Quotes and optionally escapes a string to
     * database requirements for insertion into the database
     *
     * @param   string  $text   Text to quote
     * @param   bool    $escape To escape or not
     *
     * @return  string
     */
    public function quoteValue($text, $escape = true)
    {
        if ($escape) {
            $text = $this->escapeValue($text);
        }

        $quote = $this->getQuote();

        $quotedText = $quote . $text . $quote;

        return $quotedText;
    }

    /**
     * Wraps an SQL statement identifier name such as column,
     * table or database names in quotes
     *
     * @param string $name   Text to quote
     * @param bool   $escape To escape or not
     *
     * @return  string
     */
    public function quoteName($name, $escape = true)
    {
        if ($escape) {
            $name = $this->escapeName($name);
        }

        $quote = $this->getQuoteForNames();

        $quotedName = $quote . $name . $quote;

        return $quotedName;
    }

    /**
     * Prepares query for executing. Returns prepared string
     *
     * @param string|\Core\Db\DbQuery  $query  Query to execute
     *
     * @throws \Core\Exception\SystemException
     *
     * @return string
     */
    public function prepareQuery($query)
    {
        if ($query instanceof DbQuery) {
            $query = $query->buildQuery();
        }

        return $query;
    }

}