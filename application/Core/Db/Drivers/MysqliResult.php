<?php

namespace Core\Db\Drivers;

/**
 * Class MysqliResult
 *
 * @package Core\Db\Drivers
 */
class MysqliResult extends ResultAbstract
{
    /**
     * Mysqli result instance
     *
     * @var \mysqli_result
     */
    private $_mysqliResult;

    /**
     * Returns mysqli result instance
     *
     * @return  \mysqli_result
     */
    public function getMysqliResult()
    {
        return $this->_mysqliResult;
    }

    /**
     * Sets mysqli result instance
     *
     * @param   \mysqli_result  $mysqliResult   Mysqli result instance
     */
    public function setMysqliResult($mysqliResult)
    {
        $this->_mysqliResult = $mysqliResult;
    }

    /**
     * Constructor. Saves query result resource
     *
     * @param   DbDriverAbstract    $driver         Database driver
     * @param   \mysqli_result      $mysqliResult   Mysqli result instance
     */
    public function __construct($driver, $mysqliResult)
    {
        $this->setDriver( $driver );
        $this->setMysqliResult( $mysqliResult );
    }

    /**
     * Frees the memory associated with a result
     */
    public function free()
    {
        $this->getMysqliResult()
            ->free();
    }

    /**
     * Returns names of the columns of the result set
     *
     * @return  array
     */
    public function fetchColumnNames()
    {
        return $this->getMysqliResult()
            ->fetch_fields();
    }

    /**
     * Returns first value from the first row from result set
     *
     * @return  string|false
     */
    public function fetchValue()
    {
        $row = $this->getMysqliResult()
            ->fetch_array(MYSQL_NUM);

        return reset($row);
    }

    /**
     * Returns the first row of a result set by object
     * If class name parameter is not specified, a stdClass object is returned.
     *
     * @param   string  $className  The name of the class to instantiate, set the properties of and return.
     *
     * @return  \stdClass
     */
    public function fetchRowObj($className = 'stdClass')
    {
        return $this->getMysqliResult()->fetch_object($className);
    }

    /**
     * Returns whole result set by objects
     * If class name parameter is not specified, stdClass objects are returned.
     *
     * @param   string  $className  The name of the class to instantiate, set the properties of and return.
     *
     * @return  \stdClass[]
     */
    public function fetchAllObj($className = 'stdClass')
    {
        $mysqliResult = $this->getMysqliResult();

        $objects = array();
        while ($row = $mysqliResult->fetch_object($className)) {
            $objects[] = $row;
        }

        return $objects;
    }

    /**
     * Returns the first row of a result set by associative array
     *
     * @return  array
     */
    public function fetchRowAssoc()
    {
        return $this->getMysqliResult()
            ->fetch_assoc();
    }

    /**
     * Returns whole result set by associative arrays
     *
     * @return  array[]
     */
    public function fetchAllAssoc()
    {
        $mysqliResult = $this->getMysqliResult();

        if (method_exists($mysqliResult, 'fetch_all')) {
            return $mysqliResult->fetch_all(MYSQL_ASSOC);
        } else {
            $rowSet = array();
            while ($row = $mysqliResult->fetch_assoc()) {
                $rowSet[] = $row;
            }

            return $rowSet;
        }
    }

    /**
     * Returns error message
     *
     * @return  string
     */
    public function getErrorMessage()
    {
        return $this->getDriver()
            ->getMysqli()
            ->error;
    }

    /**
     * Returns error message
     *
     * @return  int
     */
    public function getErrorNumber()
    {
        return $this->getDriver()
            ->getMysqli()
            ->errno;
    }

    /**
     * Returns result set rows count
     *
     * @return  int
     */
    public function getRowsCount()
    {
        return $this->getMysqliResult()
            ->num_rows;
    }

    /**
     * Returns number of affected rows
     *
     * @return  int
     */
    public function getAffectedRowsCount()
    {
        return $this->getDriver()
            ->getMysqli()
            ->affected_rows;
    }

    /**
     * Returns last inserted id
     *
     * @return  int|string
     */
    public function getLastInsertedId()
    {
        return $this->getDriver()
            ->getMysqli()
            ->insert_id;
    }
}