<?php

namespace Core\Db;


class DbQueryResultHandler
{
    /**
     * Database object instance
     *
     * @var Db
     */
    protected $db;

    /**
     * @var \Core\Db\Drivers\ResultAbstract
     */
    private $result;

    /**
     * Sets db object
     *
     * @param   Db  $db Database object
     *
     * @return  $this
     */
    protected function setDb($db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Returns db object
     *
     * @return  Db
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * Returns query result instance
     * 
     * @return  \Core\Db\Drivers\ResultAbstract
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Returns query result instance
     * 
     * @param   \Core\Db\Drivers\ResultAbstract  $result Query result instance
     *
     * @return  $this
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Constructor
     *
     * @param \Core\Db\Db $db             Database object instance
     * @param mixed       $resultResource Result resource
     */
    public function __construct($db, $resultResource)
    {
        $this->setDb($db);

        $driverResultClassName = $db->getDriverResultClassName($db->getDriver()->getName());

        $driverResultObject = new $driverResultClassName($db->getDriver(), $resultResource);

        $this->setResult($driverResultObject);
    }

    /**
     * Frees the memory associated with a result
     */
    public function free()
    {
        $this->getResult()
            ->free();
    }

    /**
     * Returns names of the columns of the result set
     *
     * @return  array
     */
    public function fetchColumnNames()
    {
        return $this->getResult()
            ->fetchColumnNames();
    }

    /**
     * Returns the first value of the first row of result set 
     * 
     * @return  mixed
     */
    public function fetchValue()
    {
        return $this->getResult()
            ->fetchValue();
    }

    /**
     * Returns next row from result set by object
     * If class name parameter is not specified, a stdClass object is returned.
     *
     * @param   string  $className  The name of the class to instantiate, set the properties of and return.
     *
     * @return  \stdClass
     */
    public function fetchRowObj($className = 'stdClass')
    {
        return $this->getResult()
            ->fetchRowObj($className);
    }

    /**
     * Returns whole result set by objects
     * If class name parameter is not specified, stdClass objects are returned.
     *
     * @param   string  $className  The name of the class to instantiate, set the properties of and return.
     *
     * @return  \stdClass[]
     */
    public function fetchAllObj($className = 'stdClass')
    {
        return $this->getResult()
            ->fetchAllObj($className);
    }

    /**
     * Returns next row from result set by associative array
     *
     * @return  array
     */
    public function fetchRowAssoc()
    {
        return $this->getResult()
            ->fetchRowAssoc();
    }

    /**
     * Returns whole result set by associative arrays
     *
     * @return  array[]
     */
    public function fetchAllAssoc()
    {
        return $this->getResult()
            ->fetchAllAssoc();
    }

    /**
     * Returns result set rows count
     *
     * @return  int
     */
    public function getRowsCount()
    {
        return $this->getResult()
            ->getRowsCount();
    }

    /**
     * Returns error message
     *
     * @return  string
     */
    public function getErrorMessage()
    {
        return $this->getResult()
            ->getErrorMessage();
    }

    /**
     * Returns number of affected rows
     *
     * @return  int
     */
    public function getAffectedRowsCount()
    {
        return $this->getResult()
            ->getAffectedRowsCount();
    }

}