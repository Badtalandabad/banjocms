<?php

namespace Core\Auth;

use Core\Db\DbQuery;
use Core\Repository\TableAbstract;

/**
 * Class UserTable
 *
 * Access to table 'users'
 *
 * @package Auth
 */
class UsersTable extends TableAbstract
{
    protected $tableName = 'users';

    /**
     * Constructor.
     */
    public function __construct()
    {

    }

    /**
     * Loads user from database and returns data set of the result.
     * You can use any of the parameters separately or combined.
     *
     * @param   int     $id     User id
     * @param   string  $email  User email
     * @param   string  $login  User login
     *
     * @return  null|\stdClass
     */
    public function loadUser($id = null, $email = null, $login = null)
    {
        if (is_null($id) && is_null($email) && is_null($login)) {
            return null;
        }

        $db = \SFactory::getDb();

        $query = (new DbQuery())->select()
            ->from($this->tableName);

        $where = [];
        if ($id) {
            $where[] = 'id = ' . $db->q($id);
        }
        if ($email) {
            $where[] = 'email = ' . $db->q($email);
        }
        if ($login) {
            $where[] = 'login = ' . $db->q($login);
        }

        $query->where($where);

        $result = $db->execute($query);
        if (!$result) {
            return null;
        }

        return $result->fetchRowObj();
    }

    /**
     * Updates user info
     *
     * @param   int     $id             Id
     * @param   string  $email          Email
     * @param   string  $passwordHash   Password hash
     * @param   int     $role           Role
     *
     * @return  bool
     *
     * @throws \Core\Exception\SystemException If one of the arguments os empty
     */
    public function updateUser($id, $email, $passwordHash, $role)
    {
        if (!$id || !$email  || !$passwordHash || $role > User::LOWEST_RIGHTS_ROLE) {
            $message = __METHOD__ . '. One of the arguments is empty'
                . ' id: ' . $id . ', email: ' . $email . ', hash: ' . $passwordHash . ', role: ' . $role;
            throw new \Core\Exception\SystemException($message);
        }

        $db = \SFactory::getDb();

        $values = [
            'id' => $id,
            'email' => $email,
            'password_hash' => $passwordHash,
            'role' => $role,
        ];

        $query = (new DbQuery())->update($this->getTableName(), $values);

        return (bool)$db->execute($query);
    }

    /**
     * Creates new user. Returns new user id or false
     *
     * @param   string  $email          Email
     * @param   string  $passwordHash   Password hash
     * @param   int     $role           Role
     *
     * @return  int|false
     *
     * @throws \Core\Exception\SystemException If one of the arguments os empty
     */
    public function createUser($email, $passwordHash, $role)
    {
        if (!$email || !$passwordHash || $role >= User::LOWEST_RIGHTS_ROLE) {
            $message = __METHOD__ . '. One of the arguments is empty'
                . ' email: ' . $email . ', hash: ' . $passwordHash . ', role: ' . $role;
            throw new \Core\Exception\SystemException($message);        }

        $db = \SFactory::getDb();

        $values = [
            'email' => $email,
            'password_hash' => $passwordHash,
            'role' => $role,
        ];

        $query = (new DbQuery())->insert($this->getTableName(), $values);

        $result = $db->execute($query);
        if ($result) {
            $id = null;
            if (method_exists($result->getResult(), 'getLastInsertedId')) {
                $id = $result->getResult()->getLastInsertedId();
            }
            if (!$id) {
                // One email - one account
                $query = (new DbQuery())->select('id')
                    ->from($this->getTableName())
                    ->where($db->qn('email') . ' = ' . $db->q($email));
                $result = $db->execute($query);
                if ($result) {
                    $id = $result->fetchValue();
                }
            }
            return $id;
        } else {
            return false;
        }
    }
}