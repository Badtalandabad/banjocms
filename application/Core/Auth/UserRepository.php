<?php

namespace Core\Auth;

use Core\Repository\RepositoryAbstract;

/**
 * Class UserRepository
 *
 * User repository
 */
class UserRepository extends RepositoryAbstract
{
    /**
     * Table object
     *
     * @var UsersTable
     */
    private $userTable;

    /**
     * All loaded users. Contains all users loaded from db.
     * Just to avoid loading them twice.
     *
     * @var User[]
     */
    private $users = [
        'ids' => [],
        'emails' => [],
        'logins' => [],
    ];

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->userTable = new UsersTable();
    }

    /**
     * Makes user object
     *
     * @param   \stdClass   $dataset    Set of user data
     *
     * @return  User
     */
    protected function makeUserObj($dataset)
    {
        $user = new User(
            (int)$dataset->id,
            (int)$dataset->role,
            $dataset->email,
            $dataset->login,
            $dataset->password_hash
        );

        $this->users['ids'][$user->getId()] = $user;
        $this->users['emails'][$user->getEmail()] = $user;
        $this->users['logins'][$user->getLogin()] = $user;

        return $user;
    }

    /**
     * Returns user by id. Loads it from db if needed
     *
     * @param   int $id User id
     *
     * @return  User|null
     */
    public function getUserById($id)
    {
        if (!empty($this->users['ids'][(int)$id])) {
            $user = $this->users['ids'][(int)$id];
        } elseif ($user = $this->userTable->loadUser($id)) {
            $user = $this->makeUserObj($user);
        }

        return $user;
    }

    /**
     * Returns user by email. Loads it from db if needed
     *
     * @param   string  $email  Email
     *
     * @return  User|bool
     */
    public function getUserByEmail($email)
    {
        if (!empty($this->users['emails'][(string)$email])) {
            $user = $this->users['emails'][(string)$email];
        } elseif ($user = $this->userTable->loadUser(null, $email)) {
            $user = $this->makeUserObj($user);
        }

        return $user;
    }

    /**
     * Returns user by login. Loads it from db if needed
     *
     * @param   string  $login  Login
     *
     * @return  User|bool
     */
    public function getUserByLogin($login)
    {
        if (!empty($this->users['logins'][(string)$login])) {
            $user = $this->users['logins'][(string)$login];
        } elseif ($user = $this->userTable->loadUser(null, null, $login)) {
            $user = $this->makeUserObj($user);
        }

        return $user;
    }

    /**
     * Saves user (new or existing)
     *
     * @param   User $user
     *
     * @return  bool
     */
    public function saveUser(User $user)
    {
        if ($user->getId()) {
            $result = $this->userTable->updateUser(
                $user->getId(),
                $user->getEmail(),
                $user->getPasswordHash(),
                $user->getRole()
            );
        } else {
            $id = $this->userTable->createUser(
                $user->getEmail(),
                $user->getPasswordHash(),
                $user->getRole()
            );
            if ($id && is_int($id)) {
                $user->setId($id);
            }
            $result = (bool)$id;
        }

        return $result;
    }
}