<?php

namespace Core\Auth;

class User
{
    /**
     * Roles
     *
     * SUPERADMIN - Can do whatever he wants. CMS overlord.
     * ADMIN      - Can do regular admin business.
     * REGISTERED - Regular registered user.
     * GUEST      - Not registered unknown person.
     */
    const ROLE_SUPERADMIN = 0;
    const ROLE_ADMIN = 1;
    const ROLE_REGISTERED = 2;
    const ROLE_GUEST = 3;

    const LOWEST_RIGHTS_ROLE = self::ROLE_GUEST;

    /**
     * Id of the database row
     *
     * @var int
     */
    private $id;

    /**
     * Role in the system
     *
     * @var int
     */
    private $role;

    /**
     * User's login.
     *
     * @var string
     */
    private $login;

    /**
     * User's email
     *
     * @var string
     */
    private $email;

    /**
     * Password hash
     *
     * @var string
     */
    private $passwordHash;

    /**
     * Returns id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets id
     *
     * @param   int $id Id
     *
     * @return  $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns role
     *
     * @return  int
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Sets role
     *
     * @param   int $role   Role
     *
     * @return  $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Returns login
     *
     * @return  string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Sets login
     *
     * @param   string  $login  Login
     *
     * @return  $this
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Returns email
     *
     * @return  string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets email
     *
     * @param   string  $email  Email
     *
     * @return  $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Returns password hash
     *
     * @return  string|null
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * Sets password hash
     *
     * @param   string|null $passwordHash   Password hash
     *
     * @return  $this
     */
    protected function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    /**
     * Constructor.
     *
     * @param   int|null    $id             Id
     * @param   int         $role           Role
     * @param   string|null $login          Login
     * @param   string|null $email          Email
     * @param   string|null $passwordHash   Password hash
     */
    public function __construct($id, $role, $email, $login, $passwordHash)
    {
        $this->setId($id)
            ->setRole($role)
            ->setEmail($email)
            ->setLogin($login)
            ->setPasswordHash($passwordHash);
    }

    /**
     * Sets new password. Calculates hash
     *
     * @param   string  $password   Password
     *
     * @return  $this
     */
    public function setPassword($password)
    {
        //Suggestion: to calculate cost
        //Suggestion: to use pepper
        $hash = password_hash($password, PASSWORD_DEFAULT);

        $this->setPasswordHash($hash);

        return $this;
    }

    /**
     * Checks if given password is the one that user has
     *
     * @param   string  $password   Password
     *
     * @return  bool
     */
    public function verifyPassword($password)
    {
        return password_verify($password, $this->getPasswordHash());
    }
}