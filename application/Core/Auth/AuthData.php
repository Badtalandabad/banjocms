<?php

namespace Core\Auth;

/**
 * Class AuthData
 * @package Core\Auth
 */
class AuthData
{
    /**
     * Auth session namespace
     */
    const AUTH_SESSION = 'auth';

    const USER_ID = 'userId';
    const LAST_URI = 'lastUri';

    static $instance;

    /**
     * User id
     *
     * @var int|null
     */
    private $userId;

    /**
     * Last visited URI
     *
     * @var string|null
     */
    private $lastUri;

    /**
     * Returns user id
     *
     * @return  int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Sets user id
     *
     * @param   int|null    $userId User id
     *
     * @return  $this
     */
    public function setUserId($userId)
    {
        $data = \SRequester::getSessionVariable(self::AUTH_SESSION);

        $data[self::USER_ID] = $this->userId = $userId;

        \SRequester::setSessionVariable(self::AUTH_SESSION, $data);

        return $this;
    }

    /**
     * Returns last visited uri
     *
     * @return  string|null
     */
    public function getLastUri()
    {
        return $this->lastUri;
    }

    /**
     * Sets last visited uri
     *
     * @param   string|null  $lastUrl    Last visited URL
     *
     * @return  $this
     */
    public function setLastUri($lastUrl)
    {
        $data = \SRequester::getSessionVariable(self::AUTH_SESSION);

        $data[self::LAST_URI] = $this->lastUri = $lastUrl;

        \SRequester::setSessionVariable(self::AUTH_SESSION, $data);

        return $this;
    }

    /**
     * Constructor
     */
    protected function __construct()
    {
        $data = \SRequester::getSessionVariable(self::AUTH_SESSION);

        if (!$data) {
            \SRequester::setSessionVariable(self::AUTH_SESSION, [self::USER_ID => null, self::LAST_URI => null]);
        } else {
            if (!empty($data[self::USER_ID])) {
                $this->setUserId((int)$data[self::USER_ID]);
            }
            if (!empty($data[self::LAST_URI])) {
                $this->setLastUri($data[self::LAST_URI]);
            }
        }
    }

    protected function __clone() {}

    protected function __wakeup() {}

    /**
     * Returns instance of this class
     *
     * @return  self
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Removes auth data from session. Replace class instance by null
     */
    public static function unsetInstance()
    {
        \SRequester::setSessionVariable(self::AUTH_SESSION, null);
        self::$instance = null;
    }
}