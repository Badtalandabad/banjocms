<?php

namespace Core\ViewPatch;

class ViewPatchHandler
{
    /**
     * Registered view patch names
     *
     * @var array
     */
    private $registeredPatchNames = [];

    /**
     * Patches' renderings
     *
     * @var array
     */
    private $renderedPatches = [];

    /**
     * Returns rendered view patches
     *
     * @return array
     */
    public function getRenderedViewPatches()
    {
        return $this->renderedPatches;
    }

    /**
     * Registers view patches for current unit
     *
     * @param string $unitName Unit name
     */
    public function registerViewPatches($unitName = UNIT_NAME)
    {
        // view patches loading should be here
        $patches = [];
        switch ($unitName) {
            case 'App': $patches = ['MainMenu'];
        }

        $this->registeredPatchNames = $patches;
    }

    /**
     * Processes view patches
     *
     * @param string $unitName     Unit name
     * @param bool   $autoRegister Register patches before processing or not
     */
    public function processViewPatches($unitName = UNIT_NAME, $autoRegister = true)
    {
        if ($autoRegister) {
            $this->registerViewPatches();
        }

        foreach ($this->registeredPatchNames as $patchName) {
            $rendering = $this->processViewPatch($unitName, $patchName);
            if ($rendering) {
                $this->renderedPatches[$patchName] = $rendering;
            }
        }
    }

    /**
     * Processes view patch, returns its rendering
     *
     * @param string $unitName  Unit name
     * @param string $patchName Patch name
     *
     * @return bool
     */
    public function processViewPatch($unitName, $patchName)
    {
        $patchClass = $unitName . '\\ViewPatches\\' . $patchName . '\\' . $patchName;

        if (class_exists($patchClass)) {
            $patch = new $patchClass();
            if (method_exists($patch, 'process')) {
                return $patch->process();
            }
        }

        return null;
    }
}