<?php

namespace Core\ViewPatch;

abstract class ViewPatchAbstract
{
    /**
     * Processes view patch
     *
     * @return string
     */
    abstract public function process();
}