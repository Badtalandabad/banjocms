<?php

require __DIR__ . '/../application/Core/BaseTools/baseRequire.php';

\SConfig::init();

$uri = SRequester::getUri();

//$uri
$uriParts = explode('/', explode('?', $uri)[0]);

if (count($uriParts) > 1 && !empty($uriParts[1])) {
    $unit = ucfirst($uriParts[1]);
} else {
    $unit = DEFAULT_UNIT;
}

$unitPath = \SRegistry::getUnitPath($unit);
$unitIndexPath = $unitPath . '/index.php';

if (!is_file($unitIndexPath)) {
    http_response_code(404);
    exit();
}

require $unitIndexPath;