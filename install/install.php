<?php

require '../basetools/baseRequire.php';

SConfig::init();

$db = SFactory::getDb();

$query = '
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` tinyint(4) NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `role`, `login`, `email`, `password_hash`) VALUES
	(1, 0, \'one\', \'1@1.ru\', \'$2y$10$CUemJE80jtXGWcwxUDX2yeyKHRwb.9xYC8QVixvifeufHzXsC4P2q\');
';

$db->execute( $query );